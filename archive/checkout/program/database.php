<?php

/*  F-COM CMS Front-End System
 *  File name:  database.php
 *  Author:     Scott Gray
 *  Date:       07/20/2010
 *  Desc:       Contains the model for the schools database.
 *  Type:       Included File
 */

include("definitions.php");

error_reporting(0); # There is an expected error, this let's us by pass it.

function dbConn(){
        global $connection;
        //Function to connect to the database.
        //Returns a database connection.

        $connection = mysql_connect (DB_HOST, DB_USER, DB_PASS);
        if (!$connection)
            {
                $connection = mysql_connect (DB_HOST2, DB_USER2, DB_PASS2);
                if (!$connection)
                    {

                        die("Not connected : " . mysql_error());
                    }
            }

        // Set the active mySQL database
        $db_selected = mysql_select_db(DB_NAME, $connection);
        if (!$db_selected)
            {
                die ("Can\'t use db : " . mysql_error());
            }
}

function dbKillConn(){
        global $connection;
        mysql_close($connection);
}

/*
 * END OF FILE database.php
*/
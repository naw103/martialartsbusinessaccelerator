<?php

include('class.database.php');

class Checkout extends database
{

	function get_all_countries()
	{
		# This method will return all states that are eligible
		$data = array();


		$sql = "SELECT
						*
				FROM
						Countries";

		$data = $this->Execute($sql);

		return $data;
	}

	function get_all_states($country = 1)
	{
		# This method will return all states for a given country
		$data = array();

		$sql = "SELECT
							*
				FROM
							States
				WHERE
							Country = $country";

		$data = $this->Execute($sql);


		return $data;
	}

    function get_state($statecode)
	{
		# This method will return individual state for a given country based on state id
		$data = array();

		$sql = "SELECT
							*
				FROM
							States
				WHERE
							ID = $statecode";

		$data = $this->Execute($sql);


		return $data['State'];
	}

    public function all_orders()
	{
		# This method will return all orders
		$data = array();


		$sql = "SELECT
							*
				FROM
							checkout_customers cc,
							checkout_orders co
				WHERE
							co.coupon_code <> 'FREEACCESS' AND
							cc.customer_id = co.customer_id AND
							(co.order_product_id LIKE 'MABA-%')";

		$sql = "SELECT
							*
				FROM
							checkout_customers cc,
							checkout_orders co
				WHERE
							co.coupon_code <> 'FREEACCESS' AND
							cc.customer_id = co.customer_id AND
							co.status = 'PAID' AND
							(co.order_product_id LIKE 'MABA-%')";
		
		$data = $this->Execute($sql);


		return $data;
	}

    function get_country($countrycode)
    {
        # This method will return individual state for a given country based on state id
		$data = array();

        $sql = "SELECT CountryCode
			FROM Countries
			WHERE ID = $countrycode";

	    $data = $this->Execute($sql);

        return $data['CountryCode'];
    }

    function order_details($invoice_num)
    {
        # This method will return info on the order purchased
		$data = array();

        $sql = "SELECT
	                			cc.customer_id,
								cc.firstname AS first_name,
	                			cc.lastname AS last_name,
	                			cc.city,
	                			cc.address,
	                			cc.address2,
	                			cc.zipcode,
	                			s.Code,
	                			c.CountryCode,
	                			cc.email,
	                        	cc.phone,
								co.subscription_type_id,
								co.ez_pay,
								co.ticket_cost,
								co.paid_ticket_cost,
	                			co.order_total,
	                			co.cc_name,
	                			co.cc_type,
	                        	co.cc_num,
	                			co.cc_exp_month,
	                			co.cc_exp_year,
	                			co.order_date,
	                			co.order_product_id,
		                        co.coupon_code,
		                        co.coupon_amt,
		                        co.transaction_id,
		                        co.tshirt_size,
		                        co.subscription_id,
		                        co.hear_about_us,
		                        co.hear_about_other,
		                        co.hear_about_friend
	                FROM
	                			checkout_orders co,
	                			checkout_customers cc,
	                			States s,
	                			Countries c

	                WHERE
	                			cc.state_id = s.ID AND
	                			c.ID = cc.country_id AND
	                			cc.customer_id = co.customer_id AND
	                			co.order_product_id = '$invoice_num'";

	    $data = $this->Execute($sql);

        return $data;
    }


    function get_product_info($product_id, $pay_in_full = TRUE, $ez = 0)
    {
        # This method will return all info for a particular
        # product that relats to the id being passed
        $data = array();
        $ez_data = array();

        $sql = "SELECT
                            *
                FROM
                            checkout_products cp
                WHERE
                            product_id = '$product_id'";

        $data = $this->Execute($sql);

        if(!$pay_in_full):
           
            $ez_data = $this->get_ez_options($product_id, $ez);

            $data['ez_num'] = $ez_data['num'];
            $data['ez_id'] = $ez_data['id'];
            $data['ez_interval'] = $ez_data['interval'];
            $data['ez_cost'] = $ez_data['cost'];

        endif;


        return $data;
    }

    function get_ez_options($product_id, $ez_id = '')
    {
        $data = array();

        if($ez_id == ''):

            $sql = "SELECT
                                ce.*
                    FROM
                                checkout_product_ezpay_join cpo,
                                checkout_ezpay ce
                    WHERE
                    			cpo.ezpay_id = ce.id AND
                                cpo.active = '1' AND
                                cpo.product_id = '$product_id'";

        else:

            $sql = "SELECT
                                    ce.*
                        FROM
                                    checkout_product_ezpay_join cpo,
                                    checkout_ezpay ce
                        WHERE
                                    ce.id = '$ez_id' AND
                        			cpo.ezpay_id = ce.id AND
                                    cpo.active = '1' AND
                                    cpo.product_id = '$product_id'";

        endif;

        $data = $this->Execute($sql);

        
        return $data;
    }

    function get_hear_about()
    {
        # This method will return all hear about options
        $data = array();

        $sql = "SELECT
                            *
                FROM
                            ckt_hear_about";

        $data = $this->Execute($sql);


        return $data;
    }


	function active_coupon($site_id)
	{
		$data = array();

		date_default_timezone_set('America/New_York');
		$today = date('Y-m-d');

		$sql = "SELECT
						*
			  FROM
			  			checkout_coupons cc
			  WHERE
			  			DATE(start_date) <= DATE('$today') AND
						DATE(end_date) >= DATE('$today') AND
                        site_id = '$site_id' AND
						active = 1";

		$data = $this->Execute($sql);

		return $this->getNumRows();
	}


	function apply_coupon($coupon, $ticket_cost, $dvd_cost, $guest_cost, $pay_in_full, $ez_num)
	{
		$data = array();

		$order_total = 0;
		$coupon_amt = 0;

		$order_total = $total;

		$data['coupon_amt'] = 0;
		$data['ticket_cost'] = $ticket_cost;
		$data['dvd_cost'] = $dvd_cost;
		$data['guest_cost'] = $guest_cost;


	    if($coupon['products'] == 3):

			# Apply to all products
			$t_cost = $data['ticket_cost'];
			$d_cost = $data['dvd_cost'];
			$g_cost = $data['guest_cost'];

			# apply coupon here
	        if($coupon['type'] == '$')
	    	{

				if($pay_in_full):
					$data['coupon_amt'] = $coupon['amount'];

					$coupon_amt = $coupon['amount'];
				else:
					$data['coupon_amt'] = ($coupon['amount'] / $ez_num);

					$coupon_amt = ($coupon['amount'] / $ez_num);
				endif;


				if($t_cost > $coupon_amt):
					# no problem, just take coupon off ticket
					$t_cost = $t_cost - $coupon_amt;
				else:
					# Coupon is more then ticket, must take off other items.

					$coupon_amt = $coupon_amt - $t_cost;
					$t_cost = 0;

					if(($g_cost - $coupon_amt) < 0):
						$coupon_amt = $coupon_amt - $g_cost;
						$g_cost = 0;
					else:
						$g_cost = $g_cost - $coupon_amt;
						$coupon_amt = 0;
					endif;

					if(($coupon_amt != 0) && (($d_cost - $coupon_amt) < 0)):
						$coupon_amt = $coupon_amt - $d_cost;
						$d_cost = 0;
					else:
						$d_cost = $d_cost - $coupon_amt;
					endif;



				endif;

				$data['ticket_cost'] = $t_cost;
				$data['dvd_cost']  = $d_cost;
				$data['guest_cost']  = $g_cost;

	    	}
	        else
	    	{
	    		// Percent
	            $percent = $coupon['amount'] / 100;

				$data['coupon_amt'] = ($t_cost * $percent) + ($d_cost * $percent) + ($g_cost * $percent);

					$t_cost = $t_cost - ($t_cost * $percent);
					$d_cost = $d_cost - ($d_cost * $percent);
					$g_cost = $g_cost - ($g_cost * $percent);



					if($t_cost <= 0):
						$data['ticket_cost'] = 0;
					else:
						$data['ticket_cost'] = $t_cost;
					endif;

					if($d_cost <= 0):
						$data['dvd_cost'] = 0;
					else:
						$data['dvd_cost'] = $d_cost;
					endif;

					if($g_cost <= 0):
						$data['guest_cost'] = 0;
					else:
						$data['guest_cost'] = $g_cost;
					endif;

	    	}

		else:

			# Apply to only ticket.
			$t_cost = $data['ticket_cost'];

			# apply coupon here
	        if($coupon['type'] == '$')
	    	{
				if($pay_in_full):

					$t_cost = $t_cost - $coupon['amount'];

					$data['coupon_amt'] = $coupon['amount'];
				else:
					$t_cost = $t_cost - ($coupon['amount'] / $ez_num);

					$data['coupon_amt'] = ($coupon['amount'] / $ez_num);
				endif;

				if($t_cost <= 0):
					$data['ticket_cost'] = 0;
				else:
					$data['ticket_cost'] = $t_cost;
				endif;



	    	}
	        else
	    	{
	    		// Percent
	            $percent = $coupon['amount'] / 100;


				$discount = $t_cost * $percent;

				$t_cost = $t_cost - $discount;

				$data['coupon_amt'] = $discount;

				if($t_cost <= 0):
					$data['ticket_cost'] = 0;
				else:
					$data['ticket_cost'] = $t_cost;
				endif;

	    	}


		endif;



		return $data;

	}


}


?>
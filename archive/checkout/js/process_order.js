$(document).ready(function(){
    $('select[name="upsell_dvd"]').val('0');
    calculate_total();


	var total = $('#main_total td.order_total span.cost');
    get_signup_settings();

	$('input#btn_coupon').attr('disabled', false);
	$('input[name="coupon"]').attr("readonly", false);
	$('input[name="coupon"]').val('');


	$('input#alert_ok, #alert_overlay').click(function(){
		close_alertify();
	});

    $( "#upsell" ).one( "click", function() {

        $('select[name="upsell_dvd"]').val('1');

	});

    function alertify(txt)
    {
        var overlay = $('#alert_overlay');
        var container = $('#alert_container');
        var copy = $('#alert_copy');

        overlay.height($(document).height());

         // Show POPUP, Lets Position it
		//var pos = $('#checkout').position();

		var left_pos = ($(window).width() / 2) - (container.outerWidth() / 2);

		var top_pos = ($(window).height() / 2) - (container.outerHeight() / 1.5)


		container.css({
										'left': left_pos + 'px',
										'top': top_pos + 'px',
										'position': 'fixed'
									});

        copy.html(txt);


        overlay.fadeIn('fast');
        container.fadeIn('fast');
    }

	function close_alertify()
	{
		$('#alert_overlay').hide();
		$('#alert_container').hide();
	}

	// If State DropDown is changed
	$('select[name="country"]').change(function()
	{
		var country_id = $(this).val();


		$.post(
				"ajax/get_states.php",
				{ country_id: country_id },
				function(xml)
				{
					var option = '<option value="">Please Select</option>';

					$(xml).find('response state').each(function()
					{
						var state_id = $(this).find('state_id').text();
						var state = $(this).find('state_name').text();

						option += '<option value="' + state_id + '">' + state + '</option>';

					});

					$('select[name="state"]').html(option);


				}
		);

	});

    $('#overlay').click(function(){

        $('#upsell_container').hide();
        $('#overlay').hide();

    });

    $('#upsell_container div.close').click(function(){

        $('#upsell_container').hide();
        $('#overlay').hide();

    });

    $('#upsell').click(function(){

        //if(validateForm())

		if(validateForm() && $('div#loader').is(':hidden'))
        {
            calculate_total();

            center_elem('#upsell_container');
            $('#overlay, #upsell_container').show();
        }

    });

	// Checkout Upsell Container
	$('#checkout').click(function()
	{


		//if(validateForm())
		if(validateForm() && $('div#loader').is(':hidden'))
        {
			// Contact Info
			var firstname = $('input[name="firstname"]').val();
			var lastname = $('input[name="lastname"]').val();
			var address = $('input[name="address"]').val();
			var address2 = $('input[name="address2"]').val();
			var city = $('input[name="city"]').val();
			var zipcode = $('input[name="zipcode"]').val();
			var statecode = $('select[name="state"]').val();
			var countrycode = $('select[name="country"]').val();
			var phone = $('input[name="phone"]').val();
			var email = $('input[name="email"]').val();

            // EZ & pay in full
            var pay_in_full = $('#main_total tr.ticket').attr('data-payinfull');
            var ez = $('#main_total tr.ticket').attr('data-ez');

            // Hear About
            var hear_about = $('select[name="hear_about"]').val();
            var hear_about_other = $('input[name="hear_about_other"]').val();
            var hear_about_friend = $('input[name="hear_about_friend"]').val();



			// Credit Card Info
			var cc_name = $('input[name="cc_name"]').val();
			var cc_num = $('input[name="cc_num"]').val();
			var cc_type = '';

			$('ul.cards').find('li').each(function()
			{
				if(!$(this).hasClass('off'))
				{
					cc_type = $(this).attr('class');
				}

			});

			var cc_exp_month = $('select[name="cc_exp_month"]').val();
			var cc_exp_year = $('select[name="cc_exp_year"]').val();
			var cc_code = $('input[name="cvv2"]').val();

            // Coupon Code
            var coupon_code = $('input[name="coupon"]').val();

            center_elem($('#loader'));
            $('div#overlay').fadeIn('fast');
			$('div#loader').fadeIn('fast');

			$.ajax({
					url: 'ajax/process_order.php',
					type: 'POST',
					dataType: 'xml',
					data:
					{
                        // contact info
						firstname: firstname,
						lastname: lastname,
						address: address,
						address2: address2,
						city: city,
						zipcode: zipcode,
						statecode: statecode,
						countrycode: countrycode,
						phone: phone,
						email: email,

                        // pay in full
                        pay_in_full: pay_in_full,

                        // ez options
                        ez: ez,

                        // ticket
                        ticket: 1,


                        // hear about
                        hear_about: hear_about,
                        hear_about_other: hear_about_other,
                        hear_about_friend: hear_about_friend,


						cc_name: cc_name,
						cc_num: cc_num,
						cc_type: cc_type,
						cc_exp_month: cc_exp_month,
						cc_exp_year: cc_exp_year,
						cc_code: cc_code,

                        // coupon code
                        coupon_code: coupon_code
					},
					complete: function(xml, status)
					{
                       notify_user(xml.responseText);
					}

				});

		}
		else
		{

			// This will shoot me an email with any issues that may arrive here.
			$.ajax({
					url: 'ajax/error_validate.php',
					type: 'POST',
					dataType: 'xml',
					data:
					{
						subject: 'Register Button Click',
						firstname: $('input[name="firstname"]').val(),
						lastname: $('input[name="lastname"]').val(),
						address: $('input[name="address"]').val(),
						address2: $('input[name="address2"]').val(),
						city: $('input[name="city"]').val(),
						zipcode: $('input[name="zipcode"]').val(),
						statecode: $('input[name="statecode"]').val(),
						countrycode: $('input[name="countrycode"]').val(),
						phone: $('input[name="phone"]').val(),
						email: $('input[name="email"]').val(),
						mm_option: $('input[name="mm_when"]:checked').val(),


						cc_name: $('input[name="cc_name"]').val(),
	                    coupon_code: $('input[name="coupon"]').val(),
						errors: $('.error').length
					}
			});

		}

	});


    function notify_user(xml)
    {

        var approved = 0;
        var valid_card = 0;
        var authorization_code = '';
        var transaction_id = '';
        var response_reason = '';
        var invoice_num = '';

        $(xml).find('result').each(function()
        {
            approved = Number($(this).find('approved').text());
            valid_card = Number($(this).find('valid_card').text());
            authorization_code = $(this).find('authorization_code').text();
            transaction_id = $(this).find('transaction_id').text();
            response_reason = $(this).find('response_reason').text();
            invoice_num = $(this).find('invoice_num').text();
        });


        if(approved == '1')
        {
            window.location.href = 'thankyou.php?id=' + invoice_num;
        }
        else
        {
            $('div#loader').hide();

            if(!$('#upsell_container').is(':visible'))
            {
                $('div#overlay').hide();
            }

            alertify(response_reason);
        }
    }



    function center_elem(elem)
    {

        $('div#overlay').height($(document).height());

         // Show POPUP, Lets Position it
		var pos = $('#checkout').position();

		var left_pos = ($(window).width() / 2) - ($('#loader').outerWidth() / 2);

		var top_pos = ($(window).height() / 2) - ($('#loader').outerHeight() / 1.5)

		$('#loader').css({
										'left': left_pos + 'px',
										'top': top_pos + 'px',
										'position': 'fixed'
									});



    }

    function error_msg(elem)
    {
        if(elem.next('div.error_msg').length < 1 && elem.attr('title') != '')
        {
            elem.after('<div class="error_msg">' + elem.attr('title') + '</div>');
        }
    }


	// FORM VALIDATION
	function validateForm()
	{

	   $('#form_wrapper').find('input:visible.required, select:visible.required').each(function()
	   {
			if($(this).val() == '')
			{
                var title = $(this).attr('title');

                error_msg($(this));
				$(this).addClass('error');
			    $(this).removeClass('valid');

			}
			else
			{
			   if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				var str = $.trim($('input[name="email"]').val());

				var check_at = str.indexOf('@');

				var check_dot = str.indexOf('.');

				if(check_at != -1 && check_dot != -1)
				{
					$(this).removeClass('error');
					$(this).addClass('valid');
			   		$(this).next('div.error_msg').remove();
				}
				else
				{
					$(this).removeClass('valid');
					$(this).addClass('error');


				}
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
			}
	   });

	   //validate_radio();

       if($('.error').length > 0)
       {
            return 0;

       }

       return 1;

	}




	$('input.required').keyup(function()
	{
		if($(this).val() != '')
		{
			if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				validate_email();
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
		}
		else
		{
            error_msg($(this));
			$(this).removeClass('valid');
			$(this).addClass('error');



		}

	});


	$('input.required').blur(function()
	{
		if($.trim($(this).val()) != '')
		{
			if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				validate_email();
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
		}
		else
		{
            error_msg($(this));
			$(this).removeClass('valid');
			$(this).addClass('error');



		}

	});


	function validate_email()
	{
		var str = $.trim($('input[name="email"]').val());

		var check_at = str.indexOf('@');

		var check_dot = str.indexOf('.');

		if(check_at != -1 && check_dot != -1)
		{
			$('input[name="email"]').removeClass('error');
			$('input[name="email"]').addClass('valid');
            $('input[name="email"]').next('div.error_msg').remove();     
		}
		else
		{
			$('input[name="email"]').removeClass('valid');
			$('input[name="email"]').addClass('error');

            $('input[name="email"]').next('div.error_msg').remove();
            $('input[name="email"]').after('<div class="error_msg">Invalid Email</div>');
		}
	}

	$('input[name="address2"]').keyup(function()
	{
		if($.trim($(this).val()) != '')
		{
			$(this).addClass('valid');
		}
		else
		{
			$(this).removeClass('valid');
		}

	});




	$('select.required').change(function()
	{
		if($(this).val() != '')
		{

			$(this).removeClass('error');
			$(this).addClass('valid');

			$(this).next('div.error_msg').remove();
		}
		else
		{
            error_msg($(this));
			$(this).removeClass('valid');
			$(this).addClass('error');

		}

	});


	$('span#cvv2_define').click(function() {



		var left_pos = ($(window).width() / 2) - ($('#cvv2').width() / 2);

			var top_pos = ($(window).height() / 2) - ($('#cvv2').height() / 2)

			$('#cvv2').css({
											'left': left_pos + 'px',
											'top': top_pos + 'px',
											'position': 'fixed'
										});

		$('#cvv2').show();


		$('div#cvv2_overlay').height($(document).height());
		$('div#cvv2_overlay').show();

	});


	$('input[name="firstname"], input[name="lastname"]').blur(function(){
		$('input[name="cc_name"]').val($('input[name="firstname"]').val() + ' ' + $('input[name="lastname"]').val());

	});


    $('select[name="upsell_dvd"]').change(function(){
       calculate_total();
    });


	function calculate_total()
	{
		// This method will return the order total,
		// factoring in coupons/discounts if applied.
		var elem = $('tr.order_total');
        var total = 0;
        var coupon_prod = $('#main_total tr.order_total').attr('data-couponprod');


        total = add_ticket();

        if(Number(coupon_prod) != 3)
        {
            total = apply_coupon(total);
        }

        if($('select[name="upsell_dvd"]').val() == 1)
        {
            // DVD Selected
            $('.total_container tr.dvd').show();
            total += add_dvd();



		   //	var c = Number($('tr.actual_order_total').attr('data-cost'));
			//c = c + 69;
			//$('tr.actual_order_total').find('span.cost').html(c.toFixed(2));

        }
        else
        {
            // DVD not selected
            $('.total_container tr.dvd').hide();

			//var c = Number($('tr.actual_order_total').attr('data-cost'));
			//c = c;
			//$('tr.actual_order_total').find('span.cost').html(c.toFixed(2));
        }



        if($('#main_total tr.guest').length > 0)
        {
            // Guest added
            total += add_guest();
        }


        if(Number(coupon_prod) == 3)
        {
            // Coupon applied to all products
            total = apply_coupon(total);
        }
        //total = apply_coupon(total);
        if(Number(total) <= 0)
        {

            $('tr.ticket_ez').hide();
			$('#billing_wrapper').hide();
			$('tr.actual_order_total').attr('data-cost', 0);
			$('tr.actual_order_total').find('span.cost').html('0.00');
        }
        else
        {
           $('tr.ticket_ez').show();
		   $('#billing_wrapper').show();
        }




        // update order total
        elem.attr('data-cost', total.toFixed(2));
        elem.find('span.cost').html(total.toFixed(2));



	}


    function apply_coupon(total)
    {
        // Find if coupon is applied
		var coupon_amt = $('tr.order_total').attr('data-couponamt');

		if(coupon_amt != '')
		{
			var coupon_type = $('tr.order_total').attr('data-coupontype');

			if(coupon_type == '$')
			{
                //$('tr.coupon_amt').show();

				if($('#main_total tr.ticket').attr('data-payinfull') == '1')
                {
					// Amount

					total = total - coupon_amt;
                    $('tr.coupon_amt').show();
                }
                else
                {
                    var e = $('tr.ticket').attr('data-eznum');

					var cc_total = Number($('tr.ticket').attr('data-realcost'));

					if($('select[name="upsell_dvd"]').val() == '1')
					{
						cc_total += Number($('tr.dvd').attr('data-realcost'));
					}

					if($('tr.guest').length > 0)
					{
						cc_total += Number($('tr.guest').attr('data-realcost'));
					}


					// Amount
					var actual_total = cc_total;
					cc_total = (cc_total - coupon_amt) / e;

					var discount = (actual_total / e) - cc_total;
					//total = total - (coupon_amt / e);

					$('tr.coupon_amt').find('td:eq(1)').html(num_to_money(discount) + "<span>/ payment</span>");
					$('tr.coupon_amt').show();


					$('tr.actual_order_total').attr('data-cost', (actual_total - coupon_amt));
					$('tr.actual_order_total').find('span.cost').html((actual_total - coupon_amt).toFixed(2));

					total = cc_total;
					//$('tr.ticket_ez span').html(num_to_money(total));
                    // update order total
                    //$('tr.ticket').attr('data-cost', total.toFixed(2));
                    //$('tr.ticket').find('span.cost').html(total.toFixed(2));

                }

			}
			else
			{
				// Percent
                var percent = coupon_amt / 100;

				var discount = total * percent;

                total = total - discount;

                if($('#main_total tr.ticket').attr('data-payinfull') == '1')
                {
                    $('tr.coupon_amt').find('td:eq(1)').html(num_to_money(discount));
                    $('tr.coupon_amt').show();
                }
                else
                {
					var cc_total = Number($('tr.ticket').attr('data-realcost'));

					if($('select[name="upsell_dvd"]').val() == '1')
					{
						cc_total += Number($('tr.dvd').attr('data-realcost'));
					}

					if($('tr.guest').length > 0)
					{
						cc_total += Number($('tr.guest').attr('data-realcost'));
					}

					var t = Number($('#main_total tr.ticket').attr('data-cost'));
					var e = $('tr.ticket').attr('data-eznum');

					//var t_total = t * percent;
                    //$('tr.ticket_ez span').html(num_to_money(total));
					var actual_total = cc_total;
					var discount = cc_total * percent;
					cc_total = cc_total - discount;


					$('tr.actual_order_total').attr('data-cost', (actual_total - discount));
					$('tr.actual_order_total').find('span.cost').html((actual_total - discount).toFixed(2));


                    // update order total
                    //$('tr.ticket').attr('data-cost', total.toFixed(2));
                    //$('tr.ticket').find('span.cost').html(total.toFixed(2));
                }

			}
		}
		else
		{
			var cc_total = Number($('tr.ticket').attr('data-realcost'));

			if($('select[name="upsell_dvd"]').val() == '1')
			{
				cc_total += Number($('tr.dvd').attr('data-realcost'));
			}

			if($('tr.guest').length > 0)
			{
				cc_total += Number($('tr.guest').attr('data-realcost'));
			}
			console.log(cc_total);
			$('tr.actual_order_total').attr('data-cost', (cc_total));
			$('tr.actual_order_total').find('span.cost').html((cc_total).toFixed(2));
		}

        return total;
    }

    function add_ticket()
    {
         return Number($('#main_total tr.ticket').attr('data-cost'));
	}

    function add_dvd()
    {
         return Number($('#main_total tr.dvd').attr('data-cost'));
	}

    function add_guest()
    {
         return Number($('#main_total tr.guest').attr('data-cost'));
	}


    function num_to_money(num)
    {
		if(Math.round(num) !== num) {
        	return  '$' + num.toFixed(2);
		}
		else
		{
			return  '$' + num.toFixed(2);
		}

    }

    function num_to_percent(percent)
    {
        return  percent + '%';
    }



	$('input#btn_coupon').click(function(){
		var coupon_code = $('input[name="coupon"]').val();

		$.ajax({
				url: 'ajax/check_coupon.php',
				type: 'POST',
				dataType: 'xml',
				data:
				{
					coupon_code: coupon_code
				},
				complete: function(xml, status)
				{
					notify_coupon(xml.responseText);
				}
		});


	});

    function notify_coupon(xml)
    {
        // This method will perform the steps taken after
        // finding if the coupon is active or not.

        var status = '';
        var msg = '';
        var amt = '';
        var type = '';
        var coupon_products = '';


        $(xml).find('coupon').each(function(){

            status = $(this).find('status').text();
            msg = $(this).find('msg').text();
            amt = Number($(this).find('amount').text());
            type = $(this).find('type').text();
            coupon_products = Number($(this).find('products').text());

        });


        if(status == 0)
        {
            alertify(msg);
        }
        else
        {

			$('tr.order_total').attr('data-coupontype', type);
			$('tr.order_total').attr('data-couponamt', amt);
            $('tr.order_total').attr('data-couponprod', coupon_products);

			if(type == '$')
			{
				$('tr.coupon_amt').find('td:eq(1)').html(num_to_money(amt));
				$('tr.coupon_amt').show();
			}
			else
			{
                $('tr.coupon_discount').find('td:eq(1)').html(num_to_percent(amt));
                $('tr.coupon_discount').show();
			}

			calculate_total();

			$('input#btn_coupon').attr('disabled', true);
			$('input[name="coupon"]').attr("readonly", true);
			$('input#btn_coupon').after($('<span style="color: #008000;">   <strong>COUPON APPLIED</strong></p>'));
        }

    }


    $('#signup_settings select[name="ez_payment"]').change(function(){
        get_signup_settings();
    });

    $('#signup_settings select[name="popup_guest"]').change(function(){
        get_signup_settings();
    });

    function get_signup_settings()
    {

        var ez = $('#signup_settings select[name="ez_payment"]').val();


        var popup_pay = '';
        var src = '';

        if(ez == 0)
        {
            src = 'index.php?pay=1&ez=0';
        }
        else
        {
            src = 'index.php?pay=2&ez=' + ez;
        }

        var t_height = $('#signup_settings').height();
        $('#signup_settings .right_side').height(t_height);
        $('#signup_settings .left_side').height(t_height);

        var w_height = $(document).height();
        $('#overlay').height(w_height);

        $('#signup_settings a').attr('href', src);

    }


	$('select[name="hear_about"]').change(function(){

		var val = $(this).val();

		if(val == 'From a Friend')
		{
			$('div.friend').removeClass('hidden');
			$('div.friend').addClass('show');

			$('div.other').removeClass('show');
			$('div.other').addClass('hidden');


			$('input[name="hear_about_other"]').val('');

		}
		else if(val == 'Other')
		{

			$('div.other').removeClass('hidden');
			$('div.other').addClass('show');

			$('div.friend').removeClass('show');
			$('div.friend').addClass('hidden');

			$('input[name="hear_about_friend"]').val('');
		}
		else
		{
			$('div.other').removeClass('show');
			$('div.other').addClass('hidden');

			$('div.friend').removeClass('show');
			$('div.friend').addClass('hidden');
			$('input[name="hear_about_friend"]').val('');
			$('input[name="hear_about_other"]').val('');
		}

	});


});


$(window).resize(function(){

  get_signup_settings();

});
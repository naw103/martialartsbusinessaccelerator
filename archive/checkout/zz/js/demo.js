(function ()
{
	$(function ()
	{
		$(".demo .numbers li").wrapInner('<a href="#"></a>').click(function (e)
		{
			e.preventDefault();
			return $('input[name="cc_num"]').val($(this).text()).trigger("input")
		});
		$(".vertical.maestro").hide().css(
		{
			opacity: 0
		});
		return $('input[name="cc_num"]').validateCreditCard(function (e)
		{
			if (e.card_type == null)
			{
				$(".cards li").removeClass("off");
				$('input[name="cc_num"]').removeClass("valid");

				$(".vertical.maestro").slideUp(
				{
					duration: 200
				}).animate(
				{
					opacity: 0
				}, {
					queue: !1,
					duration: 200
				});
				return
			}
			$(".cards li").addClass("off");
			$(".cards ." + e.card_type.name).removeClass("off");

			e.card_type.name === "maestro" ? $(".vertical.maestro").slideDown(
			{
				duration: 200
			}).animate(
			{
				opacity: 1
			}, {
				queue: !1
			}) : $(".vertical.maestro").slideUp(
			{
				duration: 200
			}).animate(
			{
				opacity: 0
			}, {
				queue: !1,
				duration: 200
			});
            $("#cc_type").val(e.card_type.name);
            //alert($("#card_type").val());
			//return e.length_valid && e.luhn_valid ? $('input[name="cc_num"]').removeClass("sg-error") : $('input[name="cc_num"]').addClass("sg-error")
            if(e.luhn_valid)
            {
					$('input[name="cc_num"]').addClass('valid');
						$('input[name="cc_num"]').removeClass('error');
                $('#cc_num').css('background', 'url("../images/valid.png") no-repeat scroll 98% center #FFFFFF');
				$('input[name="cc_num"]').next('div.error_msg').remove();
            }
            else
            {
					$('input[name="cc_num"]').addClass('error');
						$('input[name="cc_num"]').removeClass('valid');
                $('#cc_num').css('background', 'url("../images/error.png") no-repeat scroll 98% center #FFFFFF');
                if($('input[name="cc_num"]').attr('title') != '' && $('input[name="cc_num"]').next('div.error_msg').length == 0)
				{
					$('<div class="error_msg">' + $('input[name="cc_num"]').attr('title') + '</div>').insertAfter($('input[name="cc_num"]'));
				}
            }
		}, {
			accept: ["visa", "amex", "mastercard", "discover"]
		})
	})
}).call(this);
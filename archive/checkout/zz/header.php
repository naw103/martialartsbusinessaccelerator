<!-- BEGIN: Header -->
<header>
	<img src="images/martial-arts-business-summit.png" alt="" />

		<?php if(strpos($_SERVER['PHP_SELF'], 'index.php')): ?>
		<nav>

			<a href="#" class="nav"  data-target="speakers">Speakers</a><span>&nbsp;|&nbsp;</span>
			<a href="#" class="nav"  data-target="student_getting">Strategies</a><span>&nbsp;|&nbsp;</span>
			<!--<a href="#" class="nav" data-target="urgent_message">About</a><br />-->
			<a href="#" class="nav" data-target="schedule">Schedule</a><br />
			<a href="<?php echo $register_now; ?>" class="register">Register Now</a><span>&nbsp;|&nbsp;</span>
			<a href="#" class="nav" data-target="faq">FAQ</a><span>&nbsp;|&nbsp;</span>
			<a href="#" class="nav" data-target="hotel">Hotel</a>
		</nav>
		<?php endif; ?>
	
</header>
<!-- END: Header -->
<?php
	require('checkout/program/program.php');

		date_default_timezone_set("America/New_York");
	$now = date("Y-m-d H:i:s");
	$deadline = date("2014-06-30 00:01:00");

	$t_time = FALSE;
	if(strtotime($now) < strtotime($deadline)):
		$t_time = TRUE;
    endif;


	function detect_mobile(){
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		$mobile = 0;

		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))):
			$mobile = 1;
		endif;

		return $mobile;
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Business Accelerator by Mike Parrella</title>

	<meta property="og:title" content="Martial Arts Business Accelerator by Mike Parrella" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="https://www.martialartsbusinessaccelerator.com/popup/images/safe_image.png" />
	<meta property="og:image" content="https://www.martialartsbusinessaccelerator.com/popup/images/safe_image2.png" />
	<meta property="og:url" content="https://www.martialartsbusinessaccelerator.com/" />
	<meta property="og:description" content="The all-new AFFORDABLE coaching program from Michael Parrella. Discover how to get more students and grow your school like never before." />
	<meta property="og:site_name" content="martialartsbusinessaccelerator.com/" />


    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">

	<link href='https://fonts.googleapis.com/css?family=Lato:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="popup/css/styles.css" />




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-22', 'martialartsbusinessaccelerator.com');
  ga('send', 'pageview');

</script>


</head>

<body>



    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><i class="fa fa-bolt fa-lg"></i> MA Business Accelerator</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#story">The Story</a>
                        <li><a href="#test1">Testimonials</a>
                            <li><a href="#get">What You Get</a>
                                <li><a href="#test2">More Testimonials</a>
                                </li>
                                <li><a href="#start">Price &amp; Guarantee</a>
                                </li>
                                <li><a class="checkout" href="<?php echo $register_url; ?>" style="color:red;"><i>Sign Up Now!</i></a>
                                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
<?php if($t_time): ?>
	<div id="" style="width: 100%; height: 60px; line-height: 60px; background: #262626; z-index: 1030; position: fixed; top: 50px;  font-family: 'Lato', sans-serif; color: #FFF; text-align: center; font-size: 30px;">
	<div class="cc">LAUNCHES IN <span class="days"></span> DAYS <span class="hours"></span> HOURS <span class="minutes"></span> MINUTES <span class="seconds"></span> SECONDS</div>
</div>
<?php endif; ?>

    <div class="intro-header parallax" data-stellar-ratio="0" data-stellar-background-ratio="0" style="<?php if(!$t_time): 'margin-top: 0px;'; endif; ?>">

        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message" style="padding: 80px 0px 100px;">
                        <h1>Mike Parrella Presents:<br><i class="fa fa-bolt"></i> Martial Arts Business Accelerator</h1>
                        <h3>The brand new, revolutionary coaching program that was designed to be AFFORDABLE for schools worldwide. Read on to learn more and grab one of the LIMITED spots today.</h3>
                        <hr class="intro-divider">

                        <a class="checkout" href="<?php echo $register_url; ?>">
							<button type="button" class="btn btn-danger extra-big">Sign Up Now! <i class="fa fa-forward"></i></button>
						</a>

                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <div id="story"></div>

    <div class="content-section-b" style="border-top:none !important;margin-top:-35px;">
        <div style="background:#f8f8f8;">
        <div class="spot-warning">
                    <center>
                    <h1 style="margin-bottom:-40px;"><span style="color:#bf534d;">IMPORTANT!</span> Last year MA Business Accelerator <u>Sold Out in 12 Hours!</u></h1>
                    <h3>This year we're ONLY letting in 100 school owners. Grab your spot ASAP. This will sell out FAST!</h3>
                    <a  class="checkout" href="<?php echo $register_url; ?>"><button type="button" class="btn btn-danger extra-big">Sign Up Now! <i class="fa fa-forward"></i>
                        </button></a>
                        </center>
                        </div>
            </div>

        <div class="container">

            <div class="row">
            
                
                <div class="mycontent">
                    
                    
                    <p style="clear:both;">A letter from Mike Parrella</p>
                    <h2 class="section-heading">To my Fellow School Owner,</h2>
                    <img src="img/mike1.jpg" class="img-responsive" style="float:left;padding:10px 20px 10px 10px;">
                    <p>Each month school owners contact me from all over the world. They want to know one thing:</p>

                    <p>How much it would cost for me to be their personal biz coach. Why?</p>

                    <p><strong>Because they’ve seen the results my coaching clients get. Most increase profits by $15k - $40k+ per month... within just 1 year of working with me.</strong>
                    </p>

                    <p>In fact, you’ll see some of their stories later on this page.</p>

                    <p>You’ll see how I’ve helped schools become local celebrities, attract boatloads of students (practically on demand)...</p>

                    <p>But most importantly...</p>

                    <h3 class="text-center">Have MORE free time than ever to spend
with your friends and loved ones.</h3>


                    <p>But no matter how much I wanted to help these school owners who reached out to me… I had a small problem:</p>


                    <h3 class="text-center">They just couldn’t afford me.</h1>


                <p>Until recently, my private coaching programs has cost $18,500 per year. (That’s not even including the flights and hotels required to attend meetings.)</p> 
                    
                   
                    <h1 class="text-center">Well, I have great news for you. There's now a HIGHLY-affordable, POWERFUL solution.</h3>

                    
        <img src="img/ryan.jpg" class="img-circle img-responsive" style="float:right; margin:-10px 10px 20px;">
                    
                    
                
                    <p>
                        While consulting with my team, Ryan Healy &amp; Nick Dougherty, who coach my clients right alongside me, we figured it out.</p>

                   
                    
                    <p>We discovered how we could make our coaching more ECONOMICAL than ever.</p>

        <img src="img/nick.jpg" class="img-circle img-responsive" style="float:right;clear:both;margin:20px 10px;">
                    
                    <p><strong>That way more school owners than ever could participate and experience the incredible results.</strong></p> 
                    
                    <p>And I could
                    finally help all those people who contact me on a regular basis.</p>

 

                    <h3>Not only that…</h3>



                    <p>We made it 3 months SHORTER than my high-end coaching program!</p>
                    
                    <p><i>After all, these schools need to get from 0 to 60 ASAP.</p> 
                        
                        <p>So we had to design the program to give them a quick "jumpstart".</i></p>
                    

                    <h1 class="text-center" style="clear:both;">We Call it <i class="fa fa-bolt fa-lg"></i> Martial Arts Business Accelerator. And it has one goal:</h1>


                    <p>To help you achieve what we call the “rockstar lifestyle”. It’s a “do what I want, when I want” way of living where you teach ONLY if you truly want to.</p>

                    <p>See, when your income gets to a certain point, and your schools are automated with amazing staff using the systems we’ll show you…</p>

                    <h3><i class="fa fa-space-shuttle pull-left fa-3x" style="margin-top:-10px;"></i> You can go travel the world... and your income will STILL grow.</h3>

                    <p>That means you no longer have to teach to survive. You can teach only the classes you truly want to - or not at all. Whichever you prefer.</p>

                    <p><strong><i>No more missing family dinners, working long weekends, or missing out on the true joys of life because you have to scrape out a living. Your life will finally be in your hands.</strong>
                        </i>
                    </p>

                    <p>Sounds good? Then let me tell you about the program. But first…</p>


                    <h1 class="text-center">See what some of my coaching clients had to say
for themselves… </h3>
                    
                    <p id="test1"></p>
                    </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->

    
    <div class="content-section-a">

        <div class="container more-narrow">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">“I tried every ‘expert’ out there… But it was Mike who brought my billing from $3,000 / month to now $20,000 / month and rising.”</h2>
                    
                    <p>I'm sincerely grateful for all the help that Ryan and Michael have given, and continue to give me. Thank you!</p>
                    
                    <p class="small">David Meyer, Running Tiger Academy</p>
                    
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6 padded">
                    <img class="img-responsive img-circle" src="img/meyer.jpg" alt="">
                </div>
                
            </div>
            </div>
        </div>

            <div class="content-section-b">
                <div class="container more-narrow">
            <div class="row"><div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">“Our business just about doubled within 2 months of working with Mike. And to make all of this extra money, we now only work a total of 4-6 hours a week.”</h2>
                    
                    <p>Today, I work 4 hours a week inside the school. Kara works 6 hours. We have all the time in the world for our children, our school is helping more people than ever with our programs, and life just feels good.</p>
                
                <p class="small">Chuck & Kara Giangreco, Westchester Martial Arts Academy</p>
                    
                    
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6 padded">
                    <img class="img-responsive img-circle" src="img/chuck-kara.jpg" alt="">
                </div></div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
    
     <div class="content-section-a">

        <div class="container more-narrow">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">"I'm enrolling more members per month, building a legitimate brand and providing the best experience possible for my students."</h2>
                    
                    <p>From the webinars covering important topics (with no filler or fluff)  to the coaching calls, this program will have you smashing your goals in no time. Since joining, I've consistently had higher gross income, a happier and more productive staff, and a program that really over delivers each and every day.  </p>
                    <p class="small">Kabir Bath, Kaboom BJJ &amp; MMA</p>
                    
                    
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6 padded">
                    <img class="img-circle img-responsive" src="img/kabir.jpg" alt="">
                </div>
                
            </div>
            </div>
        </div>
    
    
     <div class="content-section-b">
         <div class="container">
         
         <div class="row" style="padding:0 0 60px;">
             
  <div class="col-md-4">
      <h3>"A SOLID program!"</h3><p><img src="img/jeff.jpg" class="img-circle" style="float:left;padding:0 10px 0 0;">When Mike's Accelerator program was rolled out, we immediately jumped in.  We were very glad to say the least. It helped us finally open and grow our 2nd location."</p>
             <p class="small">Jeff Osinki</p></div>
  <div class="col-md-4"><h3>"Billing jumped up 47% to $82,000/month!"</h3><p><img src="img/larry.jpg" class="img-circle" style="float:left;padding:0 10px 0 0;">Since the time we started we have experienced a change in the way we see and treat our students and staff. My business has also grown by 47% and Thank God continues to grow. We are proud and thankful to be part of such an awesome group. "</p>
             <p class="small">Larry Batista</p></div>
  <div class="col-md-4"><h3>"Thanks to Mike's coaching, I now have 2 locations that gross over $75k/month!"</h3><p><img src="img/walter.jpg" class="img-circle" style="float:left;padding:0 10px 0 0;">I used to have one location that brought in $12k / month. And I worked 60-70 hours per week!! Now I have TWO locations, make $75k / month, and work just 3-5 hours per week."</p>
             <p class="small">Walter Rowe</p></div>
</div>
             

             
             </div>
        
         
         
         <hr style="padding-bottom:40px;">

        <div class="container">
            
            
            

            <div class="row">
                <div class="mycontent">
                    
                    <div class="text-center padded2">
                    <a  class="checkout" href="<?php echo $register_url; ?>"><button type="button" class="btn btn-danger extra-big">Sign me up! <i class="fa fa-forward"></i>
                    </button></a>

                </div>

                <h4 class="text-center padded3 text-primary">Spots are LIMITED. Sign up now to ensure you get in.</h4>
                    
                    
                <h1 class="text-center">Pretty inspiring stories, right?</h1>


                    <p>Here’s the thing: ANY school owner can achieve these results, and even greater. You just need the right guidance and direction, a willingness to get sh*t done, and the right mindset.</p>


                    <p>I CAN help you with each of these.</p>


                    <p>And now, with my Martial Arts Business Accelerator program, it’s easier than ever.</p>



                    <h3 class="text-center">But hold on just a second…
I have a confession to make.</h3>


                    <p>With Martial Arts Business Accelerator, I have an agenda.</p>

                    <p>Yes, I want to help as many school owners as I can - and give back to the industry that’s been my home for over 30 years… But there’s more to it than that...</p>

                    <p><strong><i>I want to help you raise your income so much, you gladly sign up for my high-paying coaching program.</i></strong></p>

                    <p id="get">Fair enough? ;-)</p>

                    <h1 class="text-center">Here’s everything you’re
getting in Martial Arts Business Accelerator:</h1>
                    
<div class="what-you-get">
                    <h3 class="underlined"><i class="fa fa-check" style="color:green;"></i> LIVE Monthly Webinar</h3>

                    <p>Every month, I’m going to give you my TOP school-growing tactics. I test these tactics out myself and so do my high-paying coaching clients.</p>

                    <p>Each one is TESTED and PROVEN to work. I don’t give you ANYTHING that I haven’t verified myself. <strong>And you can watch them from any device, anywhere.</strong></p>
                    
                    <img src="img/webinar.jpg" class="img-responsive" align="center">

</div>
                    
                    <h3 class="underlined"><i class="fa fa-check" style="color:green;"></i> One PRIVATE Accountability & Action Session Per Month</h3>

                    <p>Every month, you'll have a PRIVATE call with my all-star coaching team who are all experts in different areas of growing schools.</p>

                    <p>In these calls, you’ll have your own personal problems, concerns, and challenges addressed. You’ll get CLEAR direction. They’ll help you IMPLEMENT what you learned in the webinar - and achieve incredible levels of success.</p>
                    
                     <div style="max-width:70%;margin:50px auto 0;">
                    <img src="img/nick2.jpg" class="img-rounded img-responsive" style="float:left;margin:10px 0;">
  <img src="img/ryan2.jpg" class="img-rounded img-responsive" style="float:right;margin:10px 0;">
                         </div>
                         
                         
                    <div class="what-you-get" style="clear:both;">
    </div>

<div class="what-you-get">
                    <h3 class="underlined"><i class="fa fa-check" style="color:green;"></i> NEW! One “Virtual Hot Seat” Per Month</h3>
                        <img src="img/skype.jpg" class="img-responsive" style="float:right;padding:30px 10px 0 30px;">
                    <p>Once a month you'll also have what’s called a “hot seat”. That’s where you and a small group of other Accelerator members discuss the most pressing problems you’re facing, and my team will give you powerful answers on the spot.</p>
                    
                    <p>Your fellow Accelerator members may have gone through your problem too - and you, theirs - so you can also offer each other powerful guidance and advice.</p>

                    <p>These take place over Skype Group Video Calls, so it'll feel like you're all meeting in person (without the cost of plane tickets and hotels).</p>
    </div>
                    
                    <div class="what-you-get">
                    <h3><i class="fa fa-check" style="color:green;"></i> Access to the Secret Facebook Group</h3>
                    <p id="learn">All Accelerator members have access to our secret Facebook group. Questions get asked that anyone can reply and help
                        out on. You can get quick help from your coaches by asking public questions that everyone can benefit from. And you'll benefit
                        from seeing their questions answered too.</p>
                        
                        <p>Plus you'll make friends, get an incredible sense of community &amp; accountability, and realize you're part of a success-minded
                        group headed for great things.</p>
                        
                        <img src="img/facebook.jpg" class="img-responsive">
    </div>
                    

<div class="what-you-get">
                    <h3><i class="fa fa-check" style="color:green;"></i> Email Access & Support</h3>

                    <p id="learn"><i class="fa fa-comment pull-left fa-4x"></i> In addition to your TWO Action & Accountability calls every month, you’ll also get UNLIMITED email access to the team so we can answer your questions and concerns that pop up on the fly.</p>
    </div>

                    <h1 class="text-center">Discover ground-breaking strategies for the following and more:</h1>


                    <p><i class="fa fa-check fa-lg" style="color:green;"></i> Getting new students through online marketing</p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i> How to use Facebook to actually get as many as 1 new student per day.</p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i> The secret to using Facebook GROUPS (and why you should have a MINIMUM of 4 that you use every day).</p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i>  <strong>How to run contests and events that bring in referrals like CRAZY (in one referral contest I run every year - I have someone bring in 64 REFERRALS almost every time! Other schools I work with have members who bring in 30 or more.)
    <br><br>
    <i>I’ll teach you how to run your contests so you generate amazing results, too.</i></strong>
                    </p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i> Direct mail secrets to making this “forgotten” marketing channel bring you boatloads of new students.</p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i>  <i>How to make simple alterations to your school’s interior that can boost conversions by as much as 20%.</i>
                    </p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i> The best, most organized way to track your numbers so you know exactly how much you spend, earn, net, and gross every month. You’ll also know how many new students you got, and where they came from, so you only spend your money on advertising that works.</p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i>  <strong><i>How to hire, train, and manage a ROCKSTAR STAFF who treat your school like it's their very own.</i></strong>
                    </p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i>  <i>How to instantly cut back your hours by as much as 90%… While making MORE money than ever.</i>
                    </p>

                    <p><i class="fa fa-check fa-lg" style="color:green;"></i> And more. This is honestly the tip of the iceberg.</p>

<div class="text-center padded2">
                    <a  class="checkout" href="<?php echo $register_url; ?>"><button type="button" class="btn btn-danger extra-big">Sign me up! <i class="fa fa-forward"></i>
                    </button></a>

                </div>

                <h4 class="text-center padded3 text-primary">Spots are LIMITED. Sign up now to ensure you get in.</h4>

                    <p id="test2"></p>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>


    <div class="content-section-a">

        <div class="container more-narrow">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">“I can have dinner with my family whenever I want now. And my monthly billing increased by over $15,000 (and still rising!)”</h2>

                    <p>Brett Lechtenberg, Personal Mastery Martial Arts</p>

                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6 padded-less">
                    <img class="img-responsive img-circle" src="img/brett.jpg" alt="">
                </div>

            </div>
        </div>
    </div>

    <div class="content-section-b">
        <div class="container more-narrow">
            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">“Because of Mike’s coaching, I now have 3 locations, yet I work fewer hours than I’ve ever worked in my entire life.”</h2>
                    <p>Running my school is now fun. And the formula has been simple: do what Mike and Ryan say and get results. Couldn't have done it without them. Thank you, guys.</p>
                    <p class="small">David Inman, Champion Martial Arts</p>




                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6 padded-less">
                    <img class="img-responsive img-circle" src="img/inman.jpg" alt="">
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div class="content-section-a">

        <div class="container more-narrow">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">"I now earn $22,000 per month MORE, yet my life is freer than ever. I no longer teach to survive. I teach ONLY when I want to, because it's my passion."</h2>
                    <p>Pedro Xavier, Boston TKD Academy</p>


                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6 padded-less">
                    <img class="img-responsive img-circle" src="img/pedro.jpg" alt="">
                </div>

            </div>
        </div>
    </div>
    <div id="start"></div>
    <div class="content-section-b">
        <div class="container">



            <div class="row" style="padding:0 0 60px;">
                <div class="mycontent">

                    <h1 class="text-center">How much does it cost?</h1>


                    <p>We were able to take a coaching program that costs $15,000 - $18,500 and make it MORE ECONOMICAL than ever. That’s why the cost is nowhere near those numbers.</p>

                    <p><strong>In fact, Martial Arts Business Accelerator costs only $199 / month for 9 months.</strong>
                    </p>

                    <p><i>Or save $192 when you pay for it upfront for just $1599.</i></p>



                    <h3 class="text-center">Our mission was to make it affordable for…</h3>


                    <p>… struggling or new schools full of dedicated, hard-working people.</p>

                    <p>You’re ready to take action and do what it takes - you just need some guidance and direction from someone who’s done it and helped hundreds more.</p>

                    <h3 class="text-center">If that sounds like you, then 
Martial Arts Business Accelerator
 is a perfect fit. </h3>


                    <p id="guar"></p>



                    <h1 class="text-center">And You're Covered by Our “Insane” 100% Money 
Back Guarantee:</h1>


                    <p><i class="fa fa-star fa-4x pull-left" style="color:gray;"></i> Our guarantee is simple: <strong>If you take the information, guidance, and direction you’re given... implement it to the best of your ability... and do NOT make back at least 10x your investment…</strong>
                    </p>

                    <p><i>You get your money back.</i>
                    </p>

                    <p>I’m just that confident in the tactics we teach. I use them myself in my 5 schools, and hundreds of other school owners do too. I’m confident if you put them to use, you'll be blown away by the results.</p>

                    <p>Fair enough?</p>

                    <p>Then let’s do this ;-)</p>
                    
                    <img src="img/mike-sig.png" style="padding-bottom:20px;">
                    
                    <p>P.S. Last year we launched MA Business Accelerator for the first time with 45 spots. It sold out in 12 hours. This year there's MORE demand than ever and we're ONLY letting 100 people in. Don't say I didn't warn you. This WILL sell out fast.</p>
                    <p><a href="#"><u>Click here to secure your spot now.</u></a></p>
                    
                    <div class="text-center padded2">
                    <a  class="checkout" href="<?php echo $register_url; ?>"><button type="button" class="btn btn-danger extra-big">Sign me up! <i class="fa fa-forward"></i>
                    </button></a>

                </div>

                <h4 class="text-center padded3 text-primary">Spots are LIMITED. Sign up now to ensure you get in.</h4>
                    
                    
                </div>
            

                
            </div>


        </div>
    </div>

    </div>
    <!-- /.container -->

    </div>


    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Martial Arts Business Accelerator</h2>
                </div>
                <div class="col-lg-6">
                    <h3 class="text-center">By Mike Parrella</h3>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <p class="copyright text-muted small">Copyright &copy; Parrella Consulting 2014. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
	<?php
		// looking crappy on mobile. Lets remove it on mobile devices.
		if(!detect_mobile()):
	?>
	<script src="js/jquery.stellar.js"></script>
	<script>
		$(function(){
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 30
			});
		});
		</script>
	<?php endif; ?>

<?php
	if($t_time):
		include('popup/template.php');
	endif;
?>

<script src="popup/js/jquery.plugin.min.js"></script>
	<script src="popup/js/jquery.countdown.js"></script>

	<script>
		$(function () {


			$('#defaultCountdown').countdown({
												//until: new Date(2014, 04-1, 14),
												until: new Date('06/30/2014 12:01 AM UTC'),
												timezone: -8,
												//$.countdown.UTCDate(-4, '06/30/2014 12:01 AM UTC'),

												format: 'DHMS',
												padZeroes: true,
												onTick: createClock
			});

			//var test = new Date('4/14/2014 4:30 PM UTC');
			//console.log(test.toString());
			function createClock()
			{
				// This method will take the format the time is in
				// and create a digital clock the way we want
				var days = $('#defaultCountdown span.countdown-section:eq(0) span.countdown-amount').html();
				var hours = $('#defaultCountdown span.countdown-section:eq(1) span.countdown-amount').html();
				var minutes = $('#defaultCountdown span.countdown-section:eq(2) span.countdown-amount').html();
				var seconds = $('#defaultCountdown span.countdown-section:eq(3) span.countdown-amount').html();
				console.log(hours);
				//$('#clock_container').removeClass('red');
				if(hours == 0 && minutes == 0)
				{
					//$('#clock_container').addClass('red');
				}

					var width = $(window).width();

		if(width < 650)
		{
			var content = 'LAUNCHES IN <span class="days"></span>:<span class="hours"></span>:<span class="minutes"></span>:<span class="seconds"></span>';

			$('div.cc').html(content);


		}
		else
		{
			var content = 'LAUNCHES IN <span class="days"></span> DAYS <span class="hours"></span> HOURS <span class="minutes"></span> MINUTES <span class="seconds"></span> SECONDS';

			$('div.cc').html(content);


		}


				var time_string = hours + ':' + minutes + ':' + seconds;
				console.log(days);
				//$('#clock_container').html(time_string);
				$('.days').html(days);
				$('.hours').html(hours);
				$('.minutes').html(minutes);
				$('.seconds').html(seconds);
			}

	});

	</script>




</body>

</html>

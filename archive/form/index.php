<?php

	require('program/definitions.php');
	require('program/class.database.php');
	$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

	// Set unique id
	/*
	$sql = "SELECT
						*
			FROM
						checkout_customers cc,
						checkout_orders co
			WHERE
						co.order_product_id LIKE 'MABA%' AND
						cc.customer_id = co.customer_id";
	$cc = $db->Execute($sql);

	foreach($cc as $customer):

		$lastname = md5($customer['lastname'] . $customer['customer_id']);
		$cc_id = $customer['customer_id'];

		$sql = "UPDATE
						checkout_customers
				SET
						unique_id = '$lastname'
				WHERE
						customer_id = '$cc_id'";

		$db->Execute($sql);



	endforeach;



	$valid_user = FALSE;

	$unique_id = trim($_GET['uid']);


	$sql = "SELECT
					*
			FROM
					checkout_customers cc,
					checkout_orders co
			WHERE
					co.customer_id = cc.customer_id AND
					co.order_product_id LIKE 'MABA%' AND
					cc.unique_id = '$unique_id'";

	$customer = $db->Execute($sql);

	if($db->getNumRows() > 0):
		$valid_user = TRUE;


		date_default_timezone_set('America/New_York');
		$today = date('Y-m-d H:i:s');


		$sql = "UPDATE
						checkout_customers
				SET
						maba = '$today'
				WHERE
						unique_id = '$unique_id'";

		$db->Execute($sql);
	endif;



	$db->close();
	*/

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Business Accelerator by Mike Parrella</title>


    <!-- Custom Google Web Font -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

	<link href="css/styles.css" rel="stylesheet" >

<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-22', 'martialartsbusinessaccelerator.com');
  ga('send', 'pageview');

</script>



</head>
<body>
<?php if($valid_user || TRUE): ?>
	<div class="container">

		<header>
			<img src="images/maba-logo.png" alt="" />
			<div class="clear"></div>
			(All answers will be kept confidential)
		</header>

		<div id="page_content">

			<div id="form_wrapper">

				<input type="hidden" name="unique_id" value="<?php echo $unique_id; ?>" />

				<label for="name">Name</label>
				<input type="text" id="name" name="name" value="<?php echo $customer['firstname'] . ' ' . $customer['lastname']; ?>" /><br />



				<div class="wrapper">
					<label for="most_improvement">1. What area of your business needs the most improvement?</label><br />
					<input type="checkbox" name="most_improvement" value="Marketing" /><span>&nbsp;Marketing</span><br />
					<input type="checkbox" name="most_improvement" value="Sales" /><span>&nbsp;Sales</span><br />
					<input type="checkbox" name="most_improvement" value="Staff Development" /><span>&nbsp;Staff Development</span><br />
					<input type="checkbox" name="most_improvement" value="Retention" /><span>&nbsp;Retention</span><br />
					<input type="checkbox" name="most_improvement" value="Customer Service" /><span>&nbsp;Customer Service</span><br />
					<input type="checkbox" name="most_improvement" value="Other" /><span>&nbsp;Other (Please specify)</span><br />

					<textarea name="most_improvement_other" rows="4" cols="75" style="display: none;"></textarea>

				</div>


				<div class="wrapper">
					<label for="current_member_count">2. What is your current member count? (approx # of members who have attended in the past 30 days)</label><br />

					<input type="checkbox" name="current_member_count" value="0-50" /><span>&nbsp;0-50</span><br />
					<input type="checkbox" name="current_member_count" value="51-100" /><span>&nbsp;51-100</span><br />
					<input type="checkbox" name="current_member_count" value="101-150" /><span>&nbsp;101-150</span><br />
					<input type="checkbox" name="current_member_count" value="151-200" /><span>&nbsp;151-200</span><br />
					<input type="checkbox" name="current_member_count" value="201-250" /><span>&nbsp;201-250</span><br />
					<input type="checkbox" name="current_member_count" value="251-300" /><span>&nbsp;251-300</span><br />
					<input type="checkbox" name="current_member_count" value="300+" /><span>&nbsp;300+</span><br />

				</div>



				<div class="wrapper">
					<label for="new_members">3. How many new members on average are you enrolling each month?</label><br />
					<input type="checkbox" name="new_members" value="0-5" /><span>&nbsp;0-5</span><br />
					<input type="checkbox" name="new_members" value="6-10" /><span>&nbsp;6-10</span><br />
					<input type="checkbox" name="new_members" value="11-15" /><span>&nbsp;11-15</span><br />
					<input type="checkbox" name="new_members" value="16-20" /><span>&nbsp;16-20</span><br />
					<input type="checkbox" name="new_members" value="21-25" /><span>&nbsp;21-25</span><br />
					<input type="checkbox" name="new_members" value="25+" /><span>&nbsp;25+</span><br />

				</div>


				<div class="wrapper">
					<label for="marketing_techniques">4. Please list the marketing techniques and promotions you use on a regular basis, to attract new members</label><br />
					<textarea rows="7" cols="75" name="marketing_techniques"></textarea><br />
				</div>


				<div class="wrapper">
			   		<label for="avg_gross">5. What is your average Gross Monthly Income</label><br />
					<textarea rows="7" cols="75" name="avg_gross"></textarea>
				</div>


				<div class="wrapper">
					<label for="operation_cost">6. How much does it cost you to operate your facility each month?<br />(Total Operating Expenses incl. your Salary)</label><br />
					<textarea rows="7" cols="75" name="operation_cost"></textarea>
				</div>


				<div class="wrapper">
					<label for="website">7. Do you currently have an FC Online Marketing website for your Martial Arts School?</label><br />
					<input type="checkbox" name="website" value="Yes" /><span>&nbsp;Yes</span><br />
					<input type="checkbox" name="website" value="No" /><span>&nbsp;No</span><br />
				</div>


				<div class="wrapper">
					<label for="website_traffic">8. What techniques are you using to drive traffic to your website each month? Please list type and frequency<br />
				(i.e., weekly 1/4 page print ad, daiy classified ads, weekly bombing of parking lot, etc.)</label><br />
					<textarea rows="7" cols="75" name="website_traffic"></textarea>
				</div>


				<div class="wrapper">
					<label for="weekly_hours">9. Approximately how many hours per week do you work on and in your business?</label><br />
					<select name="weekly_hours">
						<option value="">Please Select</option>
						<option value="0-10">0-10</option>
						<option value="11-20">11-20</option>
						<option value="21-30">21-30</option>
						<option value="31-40">31-40</option>
						<option value="41+">41+</option>
					</select>
				</div><br />



				<div class="wrapper">
				<label for="fulltime_employees">10. How many full time employees do you have ? (including yourself)</label><br />
				<input type="checkbox" name="fulltime_employees" value="1" /><span>&nbsp;1</span><br />
				<input type="checkbox" name="fulltime_employees" value="2" /><span>&nbsp;2</span><br />
				<input type="checkbox" name="fulltime_employees" value="3" /><span>&nbsp;3</span><br />
				<input type="checkbox" name="fulltime_employees" value="4" /><span>&nbsp;4</span><br />
				<input type="checkbox" name="fulltime_employees" value="5" /><span>&nbsp;5</span><br />
				<input type="checkbox" name="fulltime_employees" value="6" /><span>&nbsp;6</span><br />
				<input type="checkbox" name="fulltime_employees" value="More than 6" /><span>&nbsp;More than 6?</span><br />

				</div>



				<div class="wrapper">
					<label for="parttime_employees">11. How many part time employees do you have?</label><br />
				<input type="checkbox" name="parttime_employees" value="1" /><span>&nbsp;1</span><br />
				<input type="checkbox" name="parttime_employees" value="2" /><span>&nbsp;2</span><br />
				<input type="checkbox" name="parttime_employees" value="3" /><span>&nbsp;3</span><br />
				<input type="checkbox" name="parttime_employees" value="4" /><span>&nbsp;4</span><br />
				<input type="checkbox" name="parttime_employees" value="More than 4" /><span>&nbsp;More than 4?</span><br />
				</div>


				<div class="wrapper">
					<label for="fulltime_pay">12. How much money do you pay each of your full time staff members weekly?</label><br />
					<textarea rows="7" cols="75" name="fulltime_pay"></textarea><br />
				</div>


				<div class="wrapper">
					<label for="parttime_pay">13. How much money do you pay each of your part time staff members weekly?</label><br />
					<textarea rows="7" cols="75" name="parttime_pay"></textarea><br />
				</div>


				<div class="wrapper">
					<label for="yourself_pay_employee">14. How much money do you pay yourself montly as an employee?</label><br />
					<textarea rows="7" cols="75" name="yourself_pay_employee"></textarea><br />
				</div>


				<div class="wrapper">
					<label for="yourself_pay_owner">15. How much money doy ou pay yourself monthly as an owner?</label><br />
					<textarea rows="7" cols="75" name="yourself_pay_owner"></textarea><br />
				</div>


				<div class="wrapper">

				<label for="business_years">16. How many years have you been in business?</label><br />
					<select name="business_years">
					<option value="">Please Select</option>
					<option value="<1 year">less than 1 year</option>
					<option value="1-2 years">1-2 years</option>
					<option value="3-5 years">3-5 years</option>
					<option value="6-8 years">6-8 years</option>
					<option value="9-12 years">9-12 years</option>
					<option value="12 years +">12 yeras +</option>
				</select>

				</div>


				<div class="clear"></div>



				<button id="submit">Submit</button>


			</div>

		</div>

	</div>

	<footer>
		<p class="privacy" style="text-align: center; font-size: 13px; color: #383838;">Copyright &copy; Parrella Consulting <?php echo date('Y'); ?>. All Rights Reserved</p>
	</footer>

<script src="js/process_form.js"></script>

<?php else: ?>
<p style='text-align: center font-size: 25px;'>No Direct Access</p>
<?php endif; ?>

</body>

</html>

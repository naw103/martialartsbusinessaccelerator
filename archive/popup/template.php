<div class="popup_overlay"></div>

<div class="popup <?php echo ($_GET['popup'] == '0') ? 'false' : ''; ?>">


	<div id="defaultCountdown" style="display: none;"></div>
	<div class="popup_countdown">
		<div class="close">[CLOSE]</div>
		<img class="logo" src="popup/images/logo.png" alt="" />
		<div class="cc">LAUNCHES IN <span class="days"></span> DAYS <span class="hours"></span> HOURS <span class="minutes"></span> MINUTES <span class="seconds"></span> SECONDS</div>
	</div>

		<div <?php echo ($_GET['popup'] != '0') ? 'style="display: none;"' : ''; ?>>
		<p class="earlybird" style="color: #63a924;"><strong>Success! You're on the<br />Early Bird List!</strong></p>
	</div>

	<div <?php echo ($_GET['popup'] == '0') ? 'style="display: none;"' : ''; ?>>

	<div class="popup_content">
		<p class="earlybird">
			<strong>Early Bird Access</strong>
		</p>

		<p class="limited">
			<span class="block"><span>
				<strong>
					<img src="popup/images/optin-arrow.png" alt="" />
					MA Business Accelerator Has Only 100 spots available.</strong>
				</span>
			</span>
			Get on the Early Bird List for a chance to <em>sign up 6 hours <span class="block">before everyone else!</span></em>
		</p>

	</div>


	<div class="popup_form">
		<form name="optin1" method="post" action="https://www.aweber.com/scripts/addlead.pl">
			<input type="hidden" name="meta_web_form_id" value="907453393" />

			<input type="hidden" name="meta_split_id" value="" />
			<input type="hidden" name="listname" value="awlist3494845" />
			<input type="hidden" name="redirect" value="https://www.martialartsbusinessaccelerator.com/index.php?popup=0" id="redirect_f15020cfb98e2423006aeb421b436400" />
			<input type="hidden" name="meta_redirect_onlist"  value="https://www.martialartsbusinessaccelerator.com/index.php?popup=0" />
			<input type="hidden" name="meta_adtracking" value="My_Web_Form" />
			<input type="hidden" name="meta_message" value="1" />
			<input type="hidden" name="meta_required" value="email" />
			<input type="hidden" name="meta_forward_vars" value="" />
			<input type="hidden" name="meta_tooltip" value="" />

	        <input type="text" name="from" id="email" data-placeholder="Enter Primary Email" />

			<input id="submitbutton" type="submit"  name="submit" value="Give me Early Access!" />

        </form>
	</div>
	</div>

</div>

<script>


	$(document).ready(function(){

		var elem = $('#email');
		var placeholder = elem.attr('data-placeholder');

		if(elem.val() == '' || elem.val() == placeholder)
		{
			elem.val(placeholder);
			elem.addClass('placeholder');
		}


		$('#email').focus(function(){

			var placeholder = elem.attr('data-placeholder');

			if(elem.val() == placeholder)
			{
				elem.val('');
				elem.removeClass('placeholder');
			}

		});

		$('#email').blur(function(){

			var placeholder = elem.attr('data-placeholder');

			if(elem.val() == '')
			{
				elem.val(placeholder);
				elem.addClass('placeholder');
			}

		});

		var height = $(document).height();

		$('.popup_overlay').height(height);


		$('.popup div.close').click(function(){


				$('.popup').hide();
				$('.popup_overlay').hide();

		});


		$('a.checkout').click(function(e){

			e.preventDefault();

			show_popup();

		});

		setTimeout(
		  function()
		  {

		  if(!$('.popup').hasClass('false'))
			{
				show_popup();
			}
		  }, 5000);

		  if($('.popup').hasClass('false'))
			{
				show_popup();
			}

	});


	function show_popup()
	{
		var window_width = $(window).width();
		var window_height = $(window).height();


		var doc_width = $(document).width();
		var doc_height = $(document).height();

		var popup_width = $('.popup').outerWidth();
		var popup_height = $('.popup').outerHeight();

		if(window_height > 600)
		{
				$('.popup').css({
							 'top': ((window_height / 2) - (popup_height / 2) - 30) + 'px'
				});

		}
		else
		{

			$('.popup').css({
							 'top': ((window_height / 2) - (popup_height / 2)) + 'px'
							 //'top': '10%'
			});
		}


		$('.popup_overlay').height(doc_height);

		$('.popup').show();
		$('.popup_overlay').show();
	}


	$(window).resize(function(){



	});


	</script>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Business Accelerator by Mike Parrella</title>

	<meta property="og:title" content="Martial Arts Business Accelerator by Mike Parrella" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="https://www.martialartsbusinessaccelerator.com/popup/images/safe_image.png" />
	<meta property="og:image" content="https://www.martialartsbusinessaccelerator.com/popup/images/safe_image2.png" />
	<meta property="og:url" content="https://www.martialartsbusinessaccelerator.com/" />
	<meta property="og:description" content="The all-new AFFORDABLE coaching program from Michael Parrella. Discover how to get more students and grow your school like never before." />
	<meta property="og:site_name" content="martialartsbusinessaccelerator.com/" />


    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="css/landing-page.css" rel="stylesheet">

	<link href='https://fonts.googleapis.com/css?family=Lato:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="popup/css/styles.css" />




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-22', 'martialartsbusinessaccelerator.com');
  ga('send', 'pageview');

</script>


</head>

<body>



    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><i class="fa fa-bolt fa-lg"></i> MA Business Accelerator</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#story">The Story</a>
                        <li><a href="#test1">Testimonials</a>
                            <li><a href="#get">What You Get</a>
                                <li><a href="#test2">More Testimonials</a>
                                </li>
                                <li><a href="#start">Price &amp; Guarantee</a>
                                </li>
                                <li><a class="checkout" href="<?php echo $register_url; ?>" style="color:red;"><i>Sign Up Now!</i></a>
                                </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

<div class="container">
<div class="col-md-12">
<p style="text-align: center; padding-top: 80px;"><strong>PRIVACY POLICY:</strong></p>

		<p>
			We respect and are committed to protecting your privacy. We may collect
			personally identifiable information when you visit our site. We also automatically
			receive and record information on our server logs from your browser including
			your IP address, cookie information and the page(s) you visited. We will not sell
			your personally identifiable information to anyone. (And so on...)
		</p>

		<p><strong>SECURITY POLICY:</strong></p>

		<p>
			Your payment and personal information is always safe. Our Secure Sockets
Layer (SSL) software is the industry standard and among the best software
available today for secure commerce transactions. It encrypts all of your personal
information, including credit card number, name, and address, so that it cannot
be read over the internet. (Etc.)
		</p>

		<p><strong>REFUND POLICY:</strong></p>

		<p>
All refunds will be provided as a credit to the credit card used at the time of
purchase within five (5) business days upon receipt of the returned
merchandise.
		</p>

			<p><strong>Shipping Policy/Delivery Policy:</strong></p>

		<p>
Please be assured that your items will ship out within two days of purchase.
We determine the most efficient shipping carrier for your order. The carriers that
may be used are: U.S. Postal Service (USPS), United Parcel Service (UPS) or
FedEx. Sorry but we cannot ship to P.O. Boxes.
<br /><br />
If you're trying to estimate when a package will be delivered, please note the
following:
Credit card authorization and verification must be received prior to processing.
Federal Express and UPS deliveries occur Monday through Friday, excluding
holidays.
If you require express or 2 day shipping, please call us at 303.477.3361 for
charges.
		</p>
</div>
</div>

    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Martial Arts Business Accelerator</h2>
                </div>
                <div class="col-lg-6">
                    <h3 class="text-center">By Mike Parrella</h3>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
<p>
			<a href="index.php">Home</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			<a href="privacy.php">Privacy Policy</a>
		</p>
                    <p class="copyright text-muted small">Copyright &copy; Parrella Consulting 2014. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>



</body>

</html>

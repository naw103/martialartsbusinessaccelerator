<?php
	require('checkout/program/program.php');

		date_default_timezone_set("America/New_York");
	$now = date("Y-m-d H:i:s");
	$deadline = date("2015-03-23 00:12:01");
    $deadline1 = date("2015-03-23 00:05:55");

	$t_time = FALSE;
	if(strtotime($now) < strtotime($deadline)):
		$t_time = TRUE;
    endif;

    $t_time1 = FALSE;
    if(strtotime($now) < strtotime($deadline1)):
		$t_time1 = TRUE;
    endif;
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>M.A. Business Accelerator 3.0 - Now Open!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
        <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme-fire.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href='//fonts.googleapis.com/css?family=Lato:300,400%7CRaleway:100,400,300,500,600,700%7COpen+Sans:400,500,600' rel='stylesheet' type='text/css'>
    </head>
    <body>
				
		<div class="nav-container">
		    
		    
		
		    
		    <nav>
		        <div class="nav-bar">
		            <div class="module left">
		                <a href="index.html">
		                    <h5 class="uppercase">Accelerator 3.0</h5>
		                </a>
		            </div>
		            <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
		                <i class="ti-menu"></i>
		            </div>
		            <div class="module-group right">
		                <div class="module left">
		                    <ul class="menu">
		                        <li>
		                            <a href="#perks">The Perks</a>
		                        </li>
                                <li>
		                            <a href="#coaches">Meet Your Coaches</a>
		                        </li>
                                <li>
		                            <a href="#included">What You Get</a>
		                        </li>
                                <li>
		                            <a href="#price">Price &amp; Guarantee</a>
		                        </li>
		                       
		                        
		                    </ul>
		                </div>
		                
		                <div class="module widget-handle language left" style="background-color:#fc4f4f;color:white;font-weight:bold;">
		                    <ul class="menu">
		                        <li>
		                            <a href="<?php echo $register_url; ?>" style="color:white;font-weight:bold;">Let's Go!</a>
		                          
		                        </li>
		                    </ul>
		                </div>
		            </div>
		            
		        </div>
		    </nav>
		
		</div>
		
		<div class="main-container">
					
			
					
			
					
			<a id="main-headline" class="in-page-link"></a>
			
			<section class="cover fullscreen overlay parallax">
		        <div class="background-image-holder">
		            <img alt="image" class="background-image" src="img/nick-2.jpg">
		        </div>
		        <div class="container v-align-transform">
		            <div class="row">
		                <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
		                    <h1 class="mb8">Team Parrella Presents: Martial Arts Business Accelerator 3.0. <strong>Now Open for Just <s>75</s> <s>24</s> 10 More Schools!</strong></h1>
		                    <p class="lead mb48"><strong>It's here: The 1-on-1 coaching program from Team Parrella that's taken hundreds of schools from where they are now... to where they've always wanted to be.</strong> We know what it takes to grow schools. And we want to share it with you. Will you let us? Then grab one of the <s>75</s> <s>24</s> 10 spots now and let's accelerate.<br></p>
		                    <a class="mb0 btn btn-lg btn-filled" href="<?php echo $register_url; ?>">Let's Accelerate »</a>
                            
                            
		                </div>
                        
		            </div>
		            
		        </div>
		        
		    </section><a id="perks" class="in-page-link"></a>
			
			<section class="bg-secondary">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-10 col-sm-offset-1 text-center">
		                    <h3 class="uppercase color-primary mb40 mb-xs-24">a few of the many insane perks of m.a.b.a. 3.0</h3>
		                    <p class="lead">
		                        <strong>We've got a "secret" addition to M.A. Business Accelerator too.</strong> But we can't share it quite yet. Here's the bare minimum of what you get:</p>
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><a id="test-blas" class="in-page-link"></a>
			
			<section class="image-bg bg-light overlay pt160 pb160 pt-xs-80 pb-xs-80 parallax">
		        <div class="background-image-holder">
		            <img alt="image" class="background-image" src="img/intro1.jpg">
		        </div>
		        <div class="container">
		            <div class="row">
		                <div class="col-md-7 col-sm-8">
		                    <i class="icon mb16 ti ti-comment-alt icon-sm"></i>
		                    <h3 class="mb32">"From $7k in monthly billing to $17,000 per month &amp; growing. Thank you, Team Parrella."</h3>
                            <p>If anyone is one the fence about whether Team Parrella is for them - my wife and I are the first to tell you that their coaching program is amazing.</p><p>All of our success is due to them. We listen. We take action. We implement.</p><p>THANK YOU, Team Parrella.</p><p>- Blas Antonio Corrasco</p>
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><a id="1on1" class="in-page-link"></a>
			
			<section class="bg-secondary">
		        <div class="container">
		            <div class="row v-align-children">
		                <div class="col-md-4 col-sm-5 mb-xs-24">
		                    <h3>1-on-1 Coaching Means It's Personal. And Just for You.</h3>
		                    <p>
                                <strong>No one else is on your monthly phone calls. They're 100% focused on your unique situation.</strong></p><p>Whatever you're facing - we'll help you solve. Then we'll guide you to greatness.</p>
		                </div>
		                <div class="col-md-7 col-md-offset-1 col-sm-6 col-sm-offset-1 text-center">
		                    <img class="cast-shadow" alt="Screenshot" src="img/1on1.jpg">
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><a id="judgement-free" class="in-page-link"></a>
			
			<section>
		        <div class="container">
		            <div class="row v-align-children">
		                <div class="col-md-7 col-sm-6 text-center mb-xs-24">
		                    <img class="cast-shadow" alt="Screenshot" src="img/nojudge.jpg">
		                </div>
		                <div class="col-md-4 col-md-offset-1 col-sm-5 col-sm-offset-1">
		                    <h3>This is a Judgement Free Zone... and a Lot of Fun ;-)</h3>
		                    <p>If you're doing things that are hurting your success - of course we'll tell you.</p><p><strong>But we'll never judge you or make you feel bad about it.&nbsp;We're just here to support you and help you grow.</strong></p><p>And our time together isn't "super formal" or anything either.&nbsp;It feels more like friendly phone calls that are a lot of fun. (Lots of laughing &amp; smiling inside.)</p><p>But you walk away from those calls with pages of notes &amp; an air-tight game plan that'll immediately help you grow.</p>
		                    
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><a id="we-know-how-to-grow" class="in-page-link"></a>
			
			<section class="bg-secondary">
		        <div class="container">
		            <div class="row v-align-children">
		                <div class="col-md-4 col-sm-5 mb-xs-24">
		                    <h3>Get our proven game plan for making your school grow.</h3>
		                    <p>
                                You've seen the crazy numbers our coaching clients post. You've heard the amazing stories.</p><p>None of this is by accident. We just know how to grow martial arts schools.</p><p><strong>We know what it takes to get you from where you are to greatness. And we're passionate about helping you achieve that. Will you join us?</strong></p>
		                </div>
		                <div class="col-md-7 col-md-offset-1 col-sm-6 col-sm-offset-1 text-center">
		                    <img class="cast-shadow" alt="Screenshot" src="img/blueprint.jpg">
		                </div>
		            </div>
		            
		        </div>
		        
		    </section>
			
			<a id="coaches" class="in-page-link"></a>
			
			<section class="bg-dark">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-10 col-sm-offset-1 text-center">
		                    <h3 class="uppercase color-primary mb40 mb-xs-24">Meet Your head coaches</h3>
		                    <p class="lead">
		                        Nick &amp; Ryan have mastered Mike Parrella's incredible strategies for rapid school growth.</p><p class="lead">That means you're getting 1-on-1 guidance from two people who spend more time learning from Mike than just about anyone else on the planet. You get Mike's secrets for running 7-figure locations... and 8-figure businesses... in every call. Not bad, right?</p>
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><a id="nick-highlight" class="in-page-link"></a>
			
			<section class="image-bg overlay pt240 pb240 pt-xs-180 pb-xs-180">
				<div class="background-image-holder">
					<img alt="image" class="background-image" src="img/nick-2.jpg">
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<h1 class="mb0 uppercase bold italic">nick dougherty</h1>
							<h5 class="uppercase mb32">team parrella expert &amp; coach</h5>
							<p class="lead mb0">
                                “I'm obsessed with results. When schools write me, 'Nick - we grew by 100% this year thanks to you' or, 'Nick, that campaign ROCKED - thank you' - that's when I know I did my job right.</p><p class="lead mb0">”If I don't get feedback like that, I can't sleep. So I do whatever it takes to ensure you get results."</p>
						</div>
					</div>
					
				</div>
				
			</section>
			
			<a id="ryan-highlight" class="in-page-link"></a>
			
			<section class="image-bg overlay pt240 pb240 pt-xs-180 pb-xs-180">
				<div class="background-image-holder">
					<img alt="image" class="background-image" src="img/ryan2.jpg">
				</div>
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<h1 class="mb0 uppercase bold italic">ryan healy</h1>
							<h5 class="uppercase mb32">team parrella expert &amp; coach</h5>
							<p class="lead mb0">"I've been with Mike Parrella since day 1. I was there as he created all of his amazing systems - and helped him fine tune and implement them.</p> 
                                <p class="lead mb0">I've helped thousands of schools add those same systems to their schools - and the results have been, frankly, sick.</p> <p class="lead mb0">"I'm here to make sure you become our next rockstar success story. It's what I live for."</p>
						</div>
					</div>
					
				</div>
				
			</section>
            
            <a id="included" class="in-page-link"></a>
			
			<section class="page-title page-title-1 bg-dark">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-12 text-center">
		                    <h3 class="uppercase mb0">Everything that's included</h3>
		                </div>
		            </div>
		            
		        </div>
		        
		        
		    </section><section class="image-bg bg-light parallax overlay pt160 pb160 pt-xs-80 pb-xs-80">
		        <div class="background-image-holder">
		            <img alt="image" class="background-image" src="img/test2.jpg">
		        </div>
		        <div class="container">
		            <div class="row">
		                <div class="col-md-7 col-sm-8">
		                    <i class="icon mb16 ti ti-comment-alt icon-sm"></i>
		                    <h3 class="mb32">"From $16,000 / mo. to $24,000 / mo. - and I'm FINALLY taking a salary!!"</h3>
		                    <p>Thank you, Team Parrella! It was not possible without your help and without this program! I am grateful for all the great advice and for #TEAMPARRELLA!!</p>
                            <p>- Shokoufeh Darvish-Asl</p>
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><a id="whats-included" class="in-page-link"></a>
			
			<section>
				<div class="container">
					<div class="row v-align-children">
						<div class="col-sm-4 col-md-offset-1 mb-xs-24">
							<h2 class="mb64 mb-xs-32">The Full Package, Designed to Help You Grow:</h2>
							<div class="mb40 mb-xs-24" style="padding-bottom:15px;">
								<h5 class="uppercase bold mb16"><i class="fa fa-bolt fa-pull-left fa-lg" style="color:red;"></i> 1-on-1 phone call - Every Month.</h5>
								<p>
									It's just you and your Team Parrella coach.</p>
							</div>
							<div class="mb40 mb-xs-24" style="padding-bottom:15px;">
								<h5 class="uppercase bold mb16"><i class="fa fa-bolt fa-pull-left fa-lg" style="color:red;"></i> Group hot seat - every month.</h5>
								<p>
								 Live Video Skype calls with you, your Team Parrella coach, and 4 other school owners where you help each other with problems yo'u'e facing, gain support, and learn a LOT.</p><p>One of the most powerful coaching experiences you'll ever have. Guaranteed.</p>
							</div><div class="mb40 mb-xs-24" style="padding-bottom:15px;">
								<h5 class="uppercase bold mb16"><i class="fa fa-bolt fa-pull-left fa-lg" style="color:red;"></i> FREE Ticket to M.A. Business Summit 2017!</h5>
								<p>
									Pack your bags and plan your flight - your ticket to MABS is on us ($297 value!). Welcome to the most powerful even our industry has ever seen.</p>
							</div><div class="mb40 mb-xs-24" style="padding-bottom:15px;">
								<h5 class="uppercase bold mb16"><i class="fa fa-bolt fa-pull-left fa-lg" style="color:red;"></i> Ongoing help, ACCOUNTABILITY &amp; support.</h5>
								<p>
									Gain access to the Secret Accelerator Facebook Group where you'll network, share, ask questions, and learn from your fellow school owners and your Team Parrella coaches.</p><p>An awesome, tight-knit, helpful community.</p>
							</div>
							<div class="mb40 mb-xs-24" style="padding-bottom:15px;">
								<h5 class="uppercase bold mb16"><i class="fa fa-bolt fa-pull-left fa-lg" style="color:red;"></i> Monthly Facebook Ads Recorded Webinar.</h5>
								<p>
									Each month a Team Parrella Coach will provide a step by step recording for Facebook Ad Tips.</p>
							</div>
						</div>
						<div class="col-sm-5 col-sm-6 col-sm-offset-1 text-center">
							<img alt="Screenshot" src="img/group.jpg">
						</div>
					</div>
					
				</div>
				
			</section>
            
            <a id="price" class="in-page-link"></a>
            <section class="bg-secondary">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-10 col-sm-offset-1 text-center">
		                    <h3 class="uppercase color-primary mb40 mb-xs-24">Price &amp; Guarantee</h3>
		                    
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><section>
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-4" style="margin-bottom:30px;">
		                    <h2 class="number" >Just $199 / <br>month</h2>
		                    <p>
		                        Just $199 covers everything above. Here's the thing: We've proven to the industry that we know how to grow schools. So the question isn't "Is this worth $199?"</p><p>The question is: Are you ready to invest $50 / week in your own growth and success? Are you ready for the next level? Then grab one of the LIMITED spots available and let's do this.</p>
		                </div>
		                <div class="col-sm-4">
		                    <h2 class="number">Cancel Any <br>Time</h2>
		                    <p>
		                        Accelerator comes with a 30-day cancellation policy.&nbsp;No long-term contracts or anything like that.</p><p>You stay on board as long as you need, and cancel when you're ready. Cool?</p>
		                </div>
		                <div class="col-sm-4">
		                    <h2>100% Money Back Guaranteed</h2>
		                    <p>
		                        Try out Accelerator for 30 days RISK-FREE. If you don't love the program for ANY reason - let us know for a full refund.&nbsp;</p><p>>We just want to help you and make friends. If it's not the right fit - no problem, no hard feelings. Fair enough?</p>
		                </div>
                        
                        
		            </div>
		            <center><a class="mb0 btn btn-lg btn-filled" href="<?php echo $register_url; ?>">let's accelerate »</a></center>
		        </div>
		        
		    </section>
            
            <section class="bg-secondary">
		        <div class="container">
		            <div class="row mb64 mb-xs-24">
		                <div class="text-center spread-children-large col-sm-12">
		                    <img alt="Pic" src="img/nick.png">
		                    <img alt="Pic" src="img/ryan.png">
		                </div>
		            </div>
		            
		            <div class="row">
		                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 text-center">
		                    <h2>We can't wait to work with you.</h2>
		                    <p>
                                Seriously. Helping you grow and succeed is what we wake up for. </p><p>Let's work together and get you the school and life you've always wanted.&nbsp;</p><p>- Nick &amp; Ryan, Team Parrella</p>
		                    <a class="mb0 btn btn-lg btn-filled" href="<?php echo $register_url; ?>">Let's Accelerate »</a>
		                </div>
		            </div>
		            
		        </div>
		        
		    </section>
			
			<section class="cover fullscreen overlay parallax">
		        <div class="background-image-holder">
		            <img alt="image" class="background-image" src="img/home2.jpg">
		        </div>
		        <div class="container v-align-transform">
		            <div class="row">
		                <div class="col-md-10 col-md-offset-1 col-sm-12 text-center">
		                    <h1 class="mb8">Grab one of the <s>75</s> <s>24</s> 10 spots now before they're all taken.</h1>
		                    <p class="lead mb48">
		                        And start Accelerating your school... and your life... today.</p>
		                    <a class="mb0 btn btn-lg btn-filled" href="<?php echo $register_url; ?>">Let's Accelerate »</a>
		                </div>
		            </div>
		            
		        </div>
		        
		    </section><footer class="footer-2 bg-dark text-center-xs">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<!--<a href="#1on1"><img class="image-xxs fade-half" alt="Pic" src="img/logo-light.png"></a>-->
						</div>
					
						<div class="col-sm-4 text-center">
							<span class="fade-half">
								&copy; Copyright 2016 Parrella Consulting<br>
								All Rights Reserved<br>
					<p>
						<a href="index.php">Home</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
						<a href="privacy.php">Privacy Policy</a>
					</p>								
							</span>
						</div>

					
						<!--<div class="col-sm-4 text-right text-center-xs">
							<ul class="list-inline social-list">
								<li><a href="#1on1"><i class="ti-twitter-alt"></i></a></li>
								<li><a href="#1on1"><i class="ti-facebook"></i></a></li>
								<li><a href="#1on1"><i class="ti-dribbble"></i></a></li>
								<li><a href="#1on1"><i class="ti-vimeo-alt"></i></a></li>
							</ul>
						</div>-->
					</div>
				</div>
			</footer>
		</div>
		
				
	<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/flexslider.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/masonry.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
				
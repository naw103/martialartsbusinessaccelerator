<?php
//echo sha1('buddy1234'); die;
require('../archive/form/program/definitions.php');
require('../archive/form/program/class.database.php');
include('dompdf/dompdf_config.inc.php');


$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$db->open();

$cc_id = '740';

$sql = "SELECT
			mq.*
        FROM
        			maba_questionaire mq,
        			checkout_customers cc,
        			checkout_orders co
        WHERE
        			mq.customer_id = cc.customer_id AND
        			co.customer_id = cc.customer_id AND
					co.customer_id = '$cc_id'
";

$customers1 = $db->Execute($sql);
$i = 0;
$db->close();
$customers[0]  = $customers1;
foreach($customers as $customer):
    set_time_limit(100);
    $name = trim(stripslashes($customer['name']));
    echo $name;
    $most_improvement = trim(stripslashes($customer['most_improvement']));

    $most_improvement_other = trim(stripslashes($customer['most_improvement_other']));
    $current_member_count = trim(stripslashes($customer['current_member_count']));

    $new_members = trim(stripslashes($customer['new_members']));

    $marketing_techniques = trim(stripslashes($customer['marketing_techniques']));
    $avg_gross = trim(stripslashes($customer['avg_gross']));
    $operation_cost = trim(stripslashes($customer['operation_cost']));
    $website = trim(stripslashes($customer['website']));
    $website_traffic = trim(stripslashes($customer['website_traffic']));
    $weekly_hours = trim(stripslashes($customer['weekly_hours']));
    $fulltime_employees = trim(stripslashes($customer['fulltime_employees']));
    $parttime_employees = trim(stripslashes($customer['parttime_employees']));
    $fulltime_pay = trim(stripslashes($customer['fulltime_pay']));
    $parttime_pay = trim(stripslashes($customer['parttime_pay']));
    $yourself_pay_employee = trim(stripslashes($customer['yourself_pay_employee']));
    $yourself_pay_owner = trim(stripslashes($customer['yourself_pay_owner']));
    $business_years = trim(stripslashes($customer['business_years']));

    $savein = 'pdfdir/';
$file = str_replace('\'', '', str_replace('-', '', str_replace(' ', '_', strtolower(trim($name))))) . '.pdf';
$path = $savein . $file; // directory in which to save the PDF file
$myfile = fopen($path, 'w');


//    if(!file_exists($path)):


      $data = array();


      $most_improvement_array = array(
                                      'Marketing',
                                      'Sales',
                                      'Staff Development',
                                      'Retention',
                                      'Customer Service',
                                      'Other'
      );

      $current_member_count_array = array(
                                      '0-50',
                                      '51-100',
                                      '101-150',
                                      '151-200',
                                      '201-250',
                                      '251-300',
                                      '300+'
      );

      $new_members_array = array(
                                      '0-5',
                                      '6-10',
                                      '11-15',
                                      '16-20',
                                      '21-25',
                                      '25+'
      );

      $website_array = array(
                                      'Yes',
                                      'No'
      );

      $weekly_hours_array = array(
                                      '0-10',
                                      '11-20',
                                      '21-30',
                                      '31-40',
                                      '41+'
      );

      $fulltime_employees_array = array(
                                      '1',
                                      '2',
                                      '3',
                                      '4',
                                      '5',
                                      '6',
                                      'More than 6?'

      );

      $parttime_employees_array = array(
                                      '1',
                                      '2',
                                      '3',
                                      '4',
                                      'More than 4?'

      );

      $business_years_array = array(
                                      'less than 1 year',
                                      '1-2 years',
                                      '3-5 years',
                                      '6-8 years',
                                      '9-12 years',
                                      '12 years +'
      );




       $data = array();

		$j = 0;
      foreach($most_improvement_array as $val):
          $class = ($most_improvement == $val) ? 'checked' : '';
          $data[0] = $data[0] . '<div class="checkbox ' . $class . '"></div> <div class="val" style="dwidth: 130px;">' . $val . '</div>';

		  if($j == 2):
			$data[0] = $data[0] . '<div class="clear"></div>';
		  endif;

			$j++;
          $class = '';
      endforeach;


		if($most_improvement_other != ''):
		$data[0] = $data[0] . '<div class="textarea">' . $most_improvement_other . '</div>';
		endif;
			$j = 0;
      foreach($current_member_count_array as $val):
          $class = ($current_member_count == $val) ? 'checked' : '';
          $data[1] = $data[1] . '<div class="checkbox ' . $class . '"></div> <div class="val">' . $val . '</div>';

		   if($j == 3):
			$data[1] = $data[1] . '<div class="clear"></div>';
		  endif;

			$j++;

          $class = '';
      endforeach;

		$j = 0;
      foreach($new_members_array as $val):
          $class = ($new_members == $val) ? 'checked' : '';
          $data[2] = $data[2] . '<div class="checkbox ' . $class . '"></div> <div class="val">' . $val . '</div>';

		   if($j == 2):
			$data[2] = $data[2] . '<div class="clear"></div>';
		  endif;

			$j++;

          $class = '';
      endforeach;

      $data[3] = '<div class="textarea">' . $marketing_techniques . '</div>';

      $data[4] = '<div class="textarea">' . $avg_gross . '</div>';

      $data[5] = '<div class="textarea">' . $operation_cost . '</div>';

      foreach($website_array as $val):
          $class = ($website == $val) ? 'checked' : '';
          $data[6] = $data[6] . '<div class="checkbox ' . $class . '"></div> <div class="val">' . $val . '</div>';

          $class = '';
      endforeach;

      $data[7] = '<div class="textarea">' . $website_traffic . '</div>';

		$j = 0;
      foreach($weekly_hours_array as $val):
          $class = ($weekly_hours == $val) ? 'checked' : '';
          $data[8] = $data[8] . '<div class="checkbox ' . $class . '"></div> <div class="val">' . $val . '</div>';

		  if($j == 2):
			$data[8] = $data[8] . '<div class="clear"></div>';
		  endif;

			$j++;

          $class = '';
      endforeach;

		$j = 0;
      foreach($fulltime_employees_array as $val):
          $class = ($fulltime_employees == $val) ? 'checked' : '';
          $data[9] = $data[9] . '<div class="checkbox ' . $class . '"></div> <div class="val">' . $val . '</div>';

		  if($j == 2):
			$data[9] = $data[9] . '<div class="clear"></div>';
		  endif;

			$j++;

          $class = '';
      endforeach;

	$j = 0;
      foreach($parttime_employees_array as $val):
          $class = ($parttime_employees == $val) ? 'checked' : '';
          $data[10] = $data[10] . '<div class="checkbox ' . $class . '"></div> <div class="val">' . $val . '</div>';

		   if($j == 2):
			$data[10] = $data[10] . '<div class="clear"></div>';
		  endif;

			$j++;

          $class = '';
      endforeach;

      $data[11] = '<div class="textarea">' . $fulltime_pay . '</div>';

      $data[12] = '<div class="textarea">' . $parttime_pay . '</div>';

      $data[13] = '<div class="textarea">' . $yourself_pay_employee . '</div>';

      $data[14] = '<div class="textarea">' . $yourself_pay_owner . '</div>';

		$j = 0;
      foreach($business_years_array as $val):
          $class = ($business_years == $val) ? 'checked' : '';
          $data[15] = $data[15] . '<div class="checkbox ' . $class . '"></div> <div class="val">' . $val . '</div>';

			if($j == 2):
			$data[15] = $data[15] . '<div class="clear"></div>';
		  endif;

			$j++;


          $class = '';
      endforeach;
      //echo $data[3]; die;





      // the HTML content
      $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" lang="ro">
      <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8" />
      <title>MABA Questionaire</title>

      <style type="text/css">

      strong { font-weight: bold; }

      em { font-style: italic; }

	  .clear {
	    clear: both;
		display: block;
		margin: auto; }

      body,
      html {
        font-size: 14px;
        color: #000;
        font-family: Arial, Helvetica, sans-serif;
        background: #FFF; }

      #page_content {
        width: 100%;
        padding-top: 180px;
        margin-top: 100px; }

      .checkbox {
        width: 15px;
        height: 15px;
        vertical-align: middle;
        margin-bottom: 8px;
        display: inline-block;
        margin-left: 7px;
        border: 3px solid #000;
        background: #FFF; }

        .checkbox.checked { background: #000; }

      .val {
         font-size: 12px;
		 padding-top: 4px;
         vertical-align: middle;
         display: inline-block;
		 width: 130px; }

      div.textarea {
        width: 600px;
        height: auto;
        margin-left: 10px;
        border: 2px solid #000;
        padding: 7px;
		margin-bottom: 35px; }

      p { padding-top: 30px; margin-bottom: 10px; margin-top: 10px; }

      </style>
      <body>
          <div>
              <img src="../form/images/maba-logo.png" alt="" style="width: 500px; display: block; margin: auto; margin-left: 100px;"/>
          </div>
          <div id="page_content">

      			<div id="form_wrapper">

      			   <p style="font-size: 18px;">
                         Name: <span style="display: inline;  padding-right: 100px;">' . $name . '</span>
                     </p>

                     <p>
                         1. What area of your business needs the most improvement?<br />
                         '
                           . $data[0] .
                         '

                     </p>

                     <p>
                         2. What is your current member count? (approx # of members who have attended in the past 30 days)<br />
                         '
                           . $data[1] .
                         '

                     </p>

                     <p>
                         3. How many new members on average are you enrolling each month<br />
                         '
                           . $data[2] .
                         '

                     </p>

						<br />
                     <p>
                         4. Please list the marketing techniques and promotions you use on a regular basis, to attract new members<br />
                         '
                           . $data[3] .
                         '

                     </p>

                     <p>
                         5. What is your average Gross Monthly Income<br />
                         '
                           . $data[4] .
                         '

                     </p>


                      <p>
                         6. How much does it cost you to operate your facility each month?
      (Total Operating Expenses incl. your Salary)<br />
                         '
                           . $data[5] .
                         '

                     </p>


                      <p>
                         7. Do you currently have an FC Online Marketing website for your Martial Arts School?<br />
                         '
                           . $data[6] .
                         '

                     </p>

                     <p>
                         8. What techniques are you using to drive traffic to your website each month? Please list type and frequency
      (i.e., weekly 1/4 page print ad, daiy classified ads, weekly bombing of parking lot, etc.)<br />
                         '
                           . $data[7] .
                         '

                     </p>

                     <p>
                         9. Approximately how many hours per week do you work on and in your business?<br />
                         '
                           . $data[8] .
                         '

                     </p>

                     <p>
                         10. How many full time employees do you have ? (including yourself)<br />
                         '
                           . $data[9] .
                         '

                     </p>

                     <p>
                         11. How many part time employees do you have?<br />
                         '
                           . $data[10] .
                         '

                     </p>

                     <p>
                         12. How much money do you pay each of your full time staff members weekly?<br />
                         '
                           . $data[11] .
                         '

                     </p>

                     <p>
                         13. How much money do you pay each of your part time staff members weekly?<br />
                         '
                           . $data[12] .
                         '

                     </p>

                     <p>
                         14. How much money do you pay yourself montly as an employee?<br />
                         '
                           . $data[13] .
                         '

                     </p>

                     <p>
                         15. How much money doy ou pay yourself monthly as an owner?<br />
                         '
                           . $data[14] .
                         '

                     </p>

                     <p>
                         16. How many years have you been in business?<br />
                         '
                           . $data[15] .
                         '

                     </p>



      			</div>

      		</div>


          </div>
          <footer>

          </footer>
      </body></html>';

      //echo $html;
      //echo $html; die;
      // uses dompdf class to create the PDF file, save it and streams the file
      $dompdf = new DOMPDF();
      $dompdf->load_html($html);            // Loads the HTML string
      $dompdf->render();                    // Renders the HTML to PDF

      $pdf = $dompdf->output();             // gets the PDF as a string
      //echo $name . "\n" . $pdf . 'aaaaa';
      file_put_contents(($savein. $file), $pdf);           // save the pdf file on server
      //die;
      //$dompdf->stream('file.pdf');      // Streams the PDF to the client. Will open a download dialog by default



//endif;
echo $i;
if($i == 1):
 //         die;;
    endif;
 $i++;
//die;
    endforeach;
die;
?>



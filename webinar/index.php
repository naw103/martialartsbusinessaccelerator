<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="max-width=750, initial-scale=0.5">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Business Accelerator by Mike Parrella</title>

	<meta property="og:title" content="Martial Arts Business Accelerator by Mike Parrella" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="https://www.martialartsbusinessaccelerator.com/popup/images/safe_image.png" />
	<meta property="og:image" content="https://www.martialartsbusinessaccelerator.com/popup/images/safe_image2.png" />
	<meta property="og:url" content="https://www.martialartsbusinessaccelerator.com/" />
	<meta property="og:description" content="The all-new AFFORDABLE coaching program from Michael Parrella. Discover how to get more students and grow your school like never before." />
	<meta property="og:site_name" content="martialartsbusinessaccelerator.com/" />




    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="../css/landing-page.css" rel="stylesheet">

	<link href='https://fonts.googleapis.com/css?family=Lato:400,700italic,700,400italic,300italic,300' rel='stylesheet' type='text/css'>

	<script src="https://www.ilovekickboxing.com/intl_js/jquery.js?ver=1.0"></script>

	<link rel="stylesheet" href="video-js/video-js.css?ver=1.0" />
    <link rel="stylesheet" href="css/styles.css?ver=1.0" />

    <script src="video-js/video.js"></script>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-22', 'martialartsbusinessaccelerator.com');
  ga('send', 'pageview');

</script>


<style>

ul li {
  list-style-type: none;
  font-size: 17px;
  margin-bottom: 15px; }

</style>


</head>

<body>



    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><i class="fa fa-bolt fa-lg"></i> MA Business Accelerator</a>
            </div>


            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>




        <div class="container">

        <div class="row">

            <h2>
				Webinar Videos
			</h2>

			<div style="padding-top: 40px; padding-bottom: 160px;">

			<ul style="text-align: left;">
				<li><a href="webinar1.php">- Webinar 1: &ldquo;Getting Sh*t Done The Right Way&rdquo;</a></li>
				<li><a href="webinar2.php">- Webinar 2: &ldquo;Running Your Business By The Numbers&rdquo;</a></li>
				<li><a href="webinar3.php">- Webinar 3: &ldquo;Rock Star Staff&rdquo;</a></li>
				<li><a href="webinar4.php">- Webinar 4: &ldquo;How to Manage Your School's Online Reputation&rdquo;</a></li>
				<li><a href="webinar5.php">- Webinar 5: &ldquo;How to Separate yourself from the competition&rdquo;</a></li>
			</ul>

			</div>

    	</div>
    <!-- /.container -->

    </div>


    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Martial Arts Business Accelerator</h2>
                </div>
                <div class="col-lg-6">
                    <h3 class="text-center">By Mike Parrella</h3>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <p class="copyright text-muted small">Copyright &copy; Parrella Consulting 2014. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- JavaScript -->
    <script src="../js/jquery-1.10.2.js"></script>
    <script src="../js/bootstrap.js"></script>




</body>

</html>

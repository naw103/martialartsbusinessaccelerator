<?php
	date_default_timezone_set("America/New_York");
	$now = date("Y-m-d H:i:s");
	$deadline = date("2014-06-29 23:00:00");


	if(strtotime($now) < strtotime($deadline)):
		//header("Location: ../index.php");
    endif;

	$pay = $_GET['pay'];
    $ez = $_GET['ez'];


    $ticket = TRUE;


    $pay_in_full = TRUE;
    $ez_info = array();
    $show = FALSE;

	$actual_ticket_cost = 0;


    if(!isset($pay) || $pay == '' || !isset($ez) || $ez == ''):
        $show = TRUE;
    endif;


    if($pay == 2):
        $pay_in_full = FALSE;
    endif;

    require('program/program.php');
	require('program/definitions.php');
	require('program/class.checkout.php');
	require('program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

	$states = $db->get_all_states();

	$countries = $db->get_all_countries();

    $start_total = 0;

    if($ticket):
        $ticket_info = $db->get_product_info($ticket_id, $pay_in_full, $ez);

        if($pay_in_full):
            $start_total += $ticket_info['cost'];
			$total_purchase = $ticket_info['cost'];
        else:
            $start_total += $ticket_info['ez_cost'];
    		$total_purchase = $ticket_info['ez_cost'] * $ticket_info['ez_num'];
	 endif;
		$actual_ticket_cost = $total_purchase;

    endif;

    $start_total = number_format($start_total, 2);
	$coupon = $db->active_coupon($site_id);

    $hear_about_options = $db->get_hear_about();

    $ez_info = $db->get_ez_options($ticket_id);

    $db->all_orders();
    $all_orders = $db->getNumRows();

    $open_checkout = TRUE;


    if($all_orders >= $max_ppl):
        $open_checkout = FALSE;
		
    endif;
	
  
	$db->close();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Business Accelerator by Mike Parrella</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet">

    <!-- Custom Google Web Font -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Add custom CSS here -->
    <link href="../css/landing-page.css" rel="stylesheet">
	<link rel="stylesheet" href="css/styles.css"/>
<link rel="stylesheet" href="css/signup.css" />

<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-22', 'martialartsbusinessaccelerator.com');
  ga('send', 'pageview');

</script>

<style>
#terms {
    height: 250px;
    overflow-x: scroll;
    overflow-y: auto;
    width: 100%;
}

#terms p {
  font-size: 11px;
  margin: auto;
  margin-bottom: 12px;
  text-align: left; }
</style>

</head>

<body>

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../index.php"><!--<i class="fa fa-bolt fa-lg"></i>--><img class="img-responsive" src="../img/maba-icon.png" alt="" style="max-width: 56px; margin-top: -13px; display: inline;"/> MA Business Accelerator</a>
            </div>

        </div>
        <!-- /.container -->
    </nav>

    <div class="intro-header" style="background: none;">

        <div class="container">

    <?php if($open_checkout): ?>
		<!-- BEGIN: Page Content -->
	<div id="page_content">

		<div id="headline_container">

            <h1 class="futura"><i class="fa fa-bolt fa-lg"></i> Martial Arts Business Accelerator</h1>

			<p style="font-size: 22px; color: #383838; margin: 50px 20px 20px 80px;">
				Fill in the information below to get started now!
			</p>

        </div>


		<div id="form_wrapper">

			<!-- CONTACT INFO -->
			<div id="contact_wrapper">
				<div class="section futura">Contact Info</div>


				<label for="firstname">First Name:</label>
				<input type="text" name="firstname" placeholder="First Name" title="Enter Your First Name" class="large required" /><br />

				<label for="lastname">Last Name:</label>
				<input type="text" name="lastname" placeholder="Last Name" title="Enter Your Last Name" class="large required" /><br />

				<label for="address">Address:</label>
				<input type="text" name="address" placeholder=" Address" title="Enter Your Address" class="large required" /><br />

				<label for="address2">Address 2:</label>
				<input type="text" name="address2" placeholder="Address Continued" title="Enter Your Address" class="large" /><br />

				<label for="city">City:</label>
				<input type="text" name="city" placeholder="City" title="Enter Your City" class="large required" /><br />

				<label for="zipcode">Zip Code:</label>
				<input type="text" name="zipcode" placeholder="Zip Code" title="Enter Your Zip Code" class="large required" /><br />

				<label for="state">State:</label>
				<select name="state" title="Select Your State" class="large required">
					<option value="" selected="selected">Please Select</option>
					<?php
						for($i = 0; $i < sizeof($states); $i++):
					?>
					<option value="<?php echo $states[$i]['ID']; ?>" <?php echo ($i == 2) ? 'selected="selected"' : ''; ?>><?php echo $states[$i]['State']; ?></option>
					<?php
						endfor;
					?>
				</select>

				<br />

				<label for="country">Country:</label>
				<select name="country" title="Select Your Country" class="large required">
					<option value="">Please Select</option>
					<?php
						for($i = 0; $i < sizeof($countries); $i++):
					?>
					<option value="<?php echo $countries[$i]['ID']; ?>" <?php echo ($countries[$i]['ID'] == 1) ? 'selected="selected"' : ''; ?>><?php echo $countries[$i]['CountryName']; ?></option>
					<?php
						endfor;
					?>
				</select>

				<br />

				<label for="phone" >Phone:</label>
				<input type="text" name="phone" placeholder="Phone Number" title="Enter Your Phone Number" class="large required"/><br />

				<label for="email">Email:</label>
				<input type="text" name="email" placeholder="Email" title="Enter Your Email" class="large required"/><br />



                <label for="hear_about">Hear About:</label>
                <select name="hear_about" class="large required">
                    <option value="">Please Select</option>

					<option value="Direct Mail">Direct Mail</option>
					<option value="Facebook">Facebook</option>

                    <option value="From a Friend">From a Friend</option>
					<option value="Other">Other</option>
                </select>

				<div class="friend hidden">
					<label for="hear_about">Friend:</label>
					<input type="text" name="hear_about_friend" class="required" /><br />
				</div>

				<div class="other hidden">
					<label for="hear_about">Other:</label>
					<input type="text" name="hear_about_other" class="required" /><br />
				</div>




				<br />
				<br />

                <?php if($coupon): ?>
				<label for="coupon">Coupon:</label>
                <input type="text" name="coupon" />
                <input type="button" id="btn_coupon" value="Submit Coupon" />
				<br />
                <?php endif; ?>
			</div>

			<!-- BILLING INFO -->
			<div id="billing_wrapper">

				<div class="section futura">Billing Info</div>

				<div id="">


				<ul class="cards">
                	<li class="visa">Visa</li>
                	<li class="amex">American Express</li>
                	<li class="mastercard">MasterCard</li>
                	<li class="discover">Discover</li>
            	</ul>

				<label for="cc_name">Name on Card:</label>
				<input type="text" name="cc_name" class="required" title="Enter The Name on Credit Card" value="<?php echo stripslashes($customer['firstname']); ?> <?php echo stripslashes($customer['lastname']); ?>"/><br />

				<label for="cc_num">Card Number</label>
				<input type="text" name="cc_num" class="required" title="Enter Your CC Number" /><br />

				<label for="cc_exp">Expiration Date:</label>
				<select name="cc_exp_month" class="required" >
					<option value="">Month</option>
					<?php for($i = 0; $i < 12; $i++): ?>
					<option value="<?php echo $i + 1; ?>"><?php echo $i + 1; ?></option>
					<?php endfor; ?>
				</select>
				/
				<select name="cc_exp_year" class="required" title="Enter Credit Card Expiration Date">
					<option value="">Year</option>
					<?php for($i = 0; $i < 10; $i++): ?>
					<option value="<?php echo date("Y") + $i; ?>"><?php echo date("Y") + $i; ?></option>
					<?php endfor; ?>
				</select>

				<br />

				<label for="cvv2">CVV2: <!--<span id="cvv2_define">What is this?</span>--></label>
				<input type="text" name="cvv2" class="required" title="Enter Your CC CVV2"/><br />

				</div>

			</div>

			<div class="col-md-8 col-md-offset-2" style="margin-top:20px; text-align: center;">
			<div><b>Terms & Conditions</b></div>
			<div id="terms">

            <p style="padding-top: 15px;"><strong>EARNINGS AND INCOME DISCLAIMER:</strong></p>
            <p>
                Parrella Consulting Inc. ("Team Parrella") can not and does not make any
                guarantees about your ability to get results or earn any money with our ideas,
                information, tools, or strategies. What we can guarantee is your satisfaction
                with our training. We give you a 30-day 100% satisfaction guarantee on the
                products we sell, so if you are not happy for any reason with the quality of
                our training, just ask for your money back. You should know that all products
                and services by our company are for educational and informational purposes only.
                Nothing on this page, any of our websites, or any of our content or curriculum
                is a promise or guarantee of results or future earnings, and we do not offer
                any legal, medical, tax or other professional advice. Any financial numbers
                referenced here, or on any of our sites, are illustrative of concepts only
                and should not be considered average earnings, exact earnings, or promises for
                actual or future performance. Use caution and always consult your accountant,
                lawyer or professional advisor before acting on this or any information related
                to a lifestyle change or your business or finances. You alone are responsible
                and accountable for your decisions, actions and results in life, and by your
                registration here you agree not to attempt to hold us liable for your decisions,
                actions or results, at any time, under any circumstance.
            </p>

			<p>
				<strong>Events</strong>
			</p>

			<p>
				One day events can only be attended by active clients - not clients that cancel. You must also pay three months for MABA after the one day events before you can cancel.<br><br>
				<p>
				One day events can only be attended by active clients - not clients that cancel. You must also pay three months for MABA after the one day events before you can cancel.<br><br>
				MABS(Martial Arts Business Summit) - Martial Arts Business Acceleartor (MABA) clients receive 1 free ticket to MABS, your MABA membership must remain active and paid current from the date the coupon code is redeemed thru the date of the event. If you attend MABS using your free ticket, your account must also remain active and current for 2 billing cycles after the event or you will be charged at total of $998.
			</p>
			</p>



            <p><strong>Cancellation Policy:</strong></p>
            <p>
                You have THIRTY (30) Days from the date of the
                original purchase to request a refund for your M.A. Business Acceleartor monthly
                subscription. Any refund request after the THIRTY (30) DAY time limit will
                not be processed. <strong>After the first 30 days, a 30 day cancellation notice is required.</strong>
            </p>

            <p>
                Excessive returns will not be accepted. Refunds will not be given to any one
                customer for one specific product more than twice. There is an unlimited amount
                of orders that a customer can place for one specific product, but the money
                back guarantee is void after a customer has used it for one specific product
                more than twice.
            </p>


            <p><strong>Billing Address</strong></p>
            <p>
                Billing address must be the same as the shipping address. Due to credit
                card procedures, we cannot ship to a different address than we bill to.
            </p>



            <p><strong>Terms of Subscription Renewal</strong></p>
            <p>
                Monthly M.A. Business Acceleartor Subscriptions automatically renew each month at the
                stated retail price until the subscription buyer notifies the company that
                they wish to cancel. After the first 30 days, a 30 day cancellation notice is required. Please refer to Returns & Subscription Cancellations
                section of the Terms of Service page.
            </p>

			<p><strong>Hours of Operation</strong></p>
            <p>
                Parrella Consulting Inc. ("Team Parrella") corporate office is open Monday - Friday 9:30a.m. - 5:30p.m. EST<br/>
                Parrella Consulting's shipping department is open Monday -Friday 9:30a.m. - 5:30p.m. EST<br/>
                We close for all government observed holidays
            </p>
            <p>
                This policies and procedures applies to all Parrella Consulting products
                sold on this site or any other site owned by Parrella Consulting that
                includes a link to this page.
            </p>
            <p>
                M.A. Business Acceleartor <a href="http://www.martialartsbusinessaccelerator.com">www.martialartsbusinessaccelerator.com</a>
            </p>

            <p><strong>Customer Service:</strong></p>
            <p>
                If you have questions or comments regarding Parrella Consulting, Inc. products,
                please email us at <a href="mailto:support@parrellaconsulting.com">support@parrellaconsulting.com</a>
                or call 516-543-0041
            </p>
                </div>

			</div>

			<div id="main_total" class="total_container">

				<table cellpadding="0" cellspacing="0">

                    <?php if($ticket && $pay_in_full): ?>
					<tr class="ticket" data-payinfull="<?php echo $pay; ?>"  data-ez="" data-cost="<?php echo $ticket_info['cost']; ?>" data-realcost="<?php echo $actual_ticket_cost; ?>">
						<td><?php echo $ticket_info['abbr_name']; ?>:</td>
						<td>$<span class="cost"><?php echo $ticket_info['cost']; ?></span></td>
					</tr>
                    <?php endif; ?>

                    <?php if($ticket && !$pay_in_full): ?>
					<tr class="ticket" data-payinfull="<?php echo $pay; ?>" data-ez="<?php echo $ticket_info['ez_id']; ?>" data-cost="<?php echo $ticket_info['ez_cost']; ?>" data-eznum="<?php echo $ticket_info['ez_num']; ?>" data-realcost="<?php echo $actual_ticket_cost; ?>">
						<td><?php echo $ticket_info['abbr_name']; ?>:</td>
						<td>$<span class="cost"><?php echo number_format($actual_ticket_cost, 2); ?></span></td>
					</tr>
                    <?php endif; ?>







                    <tr class="coupon_discount hidden">
                         <td>Coupon Discount:</td>
                         <td></td>
                     </tr>

                     <tr class="coupon_amt hidden">
                         <td>Coupon Amount:</td>
                         <td></td>
                     </tr>

                     <?php if(!$pay_in_full): ?>
					 <tr class="order_total payments" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="" style="background: none; color: #333333;">
                        <td colspan="2">
                            (<?php echo $ticket_info['ez_num']; ?> payments of:


                        $<span class="cost"><?php echo $total_purchase; ?></span>)</td>
					</tr>
                    <?php endif; ?>


					<tr <?php echo ($pay_in_full) ? 'class="order_total"' : 'class="actual_order_total"'; ?> data-cost="<?php echo $total_purchase; ?>" data-couponprod="" data-couponamt="" data-coupontype="" style="background: #AAAAAA;">
						<td>
                            Order Total:
                        </td>
                        <td class="cost">
                            $<span class="cost"><?php echo $total_purchase; ?></span>
                        </td>
                    </tr>
                     <?php if(!$pay_in_full): ?>
					 <tr class="order_total" data-cost="<?php echo $start_total; ?>" data-couponprod="" data-couponamt="" data-coupontype="">
                        <td>
                            Total paid today:

                        </td>
						<td>

                        $<span class="cost"><?php echo $start_total; ?></span></td>
					</tr>
                    <?php endif; ?>
				</table>

			</div>

			<button id="checkout" class="btn btn-danger" style="margin-left: 223px; margin-top: 20px;" />
			Count me in! <i class="fa fa-forward"></i>
            </button>



							<div style="display: block; text-align: center; margin: auto; margin-top: 80px;">

					<!-- (c) 2005, 2013. Authorize.Net is a registered trademark of CyberSource Corporation -->
					<div class="AuthorizeNetSeal" style="display: inline-block; margin: auto 15px;">
						<script type="text/javascript" language="javascript">var ANS_customer_id="428d52b0-b883-4656-b268-2071069a7553";</script>
						<script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script>
						<a href="https://www.authorize.net/" id="AuthorizeNetText" target="_blank">Online Payment Processing</a>
					</div>
				<!--
					<div style="display: inline-block; margin: auto 15px;">

						<script type="text/JavaScript">
							//<![CDATA[
							var sealServer=document.location.protocol+"//seals.websiteprotection.com/sealws/51b6bce3-7adc-4cd0-a02a-445bcbdd56cb.gif";var certServer=document.location.protocol+"//certs.websiteprotection.com/sealws/?sealId=51b6bce3-7adc-4cd0-a02a-445bcbdd56cb";var hostName="atamartialartsclasses.com";document.write(unescape('<div style="text-align:center;margin:0 auto;"><a target="_blank" href="'+certServer+'&pop=true" style="display:inline-block;"><img src="'+sealServer+'" alt="Website Protection&#153; Site Scanner protects this website from security threats." title="This Website Protection site seal is issued to '+ hostName +'. Copyright &copy; 2013, all rights reserved."oncontextmenu="alert(\'Copying Prohibited by Law\'); return false;" border="0" /></a><div id="bannerLink"><a href="https://www.godaddy.com/" target="_blank">Go Daddy</a></div></div>'));
							//]]>
						</script>


					</div>
					-->
				</div>


		</div>

		</div>
            <?php else: ?>
            <h4 style="font-size: 26px; color: #000; padding-bottom: 40px;">Registration is now closed.</h4>

            <?php endif; ?>
        </div>


    </div>
    <!-- /.container -->




    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Martial Arts Business Accelerator</h2>
                </div>
                <div class="col-lg-6">
                    <h3 class="text-center">By Mike Parrella</h3>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <p class="copyright text-muted small">Copyright &copy; Parrella Consulting 2014. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

<?php if($open_checkout): ?>
<div id="overlay" <?php echo $show ? 'class="show"' : ''; ?>></div>
<?php include('popup.php'); ?>

<div id="loader">

    <img src="images/loader.gif" alt="Processing" />
	<div>Processing Payment...</div>
	<div style="color: red; font-weight: none; font-size: 15px; margin-top: 7px;">
		Do Not Leave This Page While Order is Processing.<br />
		(May Take Up to 2 Minutes)
	</div>

</div>

<div id="alert_overlay"></div>
<div id="alert_container">
    <div id="alert_copy">

    </div>
    <input type="button" id="alert_ok" value="OK" />
</div>
<?php endif; ?>

    <script src="../js/bootstrap.js"></script>
	<script src="js/jquery.creditCardValidator.js"></script>
<script src="js/demo.js"></script>
<script src="js/process_order.js"></script>
</body>

</html>

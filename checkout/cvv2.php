<!-- What is cvv2 -->
<div id="cvv2_overlay"></div>
<div id="cvv2">
	<div class="close">X</div>
	<h1>What is CVV2?</h1>

	<p>
        The <strong>Card Code</strong>, sometimes called the CVV2, CVC2, CID or CVN, is a
		<strong>3-digit</strong> code located on the back of your credit card, on the
		signature strip just to the right of the card number.
	</p>

	<img src="images/cvv2.jpg" alt="" />

	<p>
		On <strong>American Express</strong> cards, the 4-digit code is on the front of
		the card, just above and to the right of the embossed card number.
	</p>



</div>
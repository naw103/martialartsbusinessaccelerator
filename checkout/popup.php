<!-- BEGIN: Pop Up -->
<div id="signup_settings" class="popup <?php echo $show ? 'show' : ''; ?>">


				<!--<div class="close">X</div>-->


				<div class="section futura-condensed" style="text-indent: 30px; font-size: 21px; text-transform: uppercase; margin-bottom: 17px; padding-top: 30px;">1. Do you want The EZ Payment Option?</div>

				<p class="text">
					The EZ Option lets you pay in multiple payments. Each payment is 30 days apart.
				</p>
			<div style="text-align: center; padding-top: 20px;">
				<select name="ez_payment">
					<option value="0">I Prefer One Payment of $<?php echo $ticket_info['cost']; ?>  (A $192 savings!)</option>

                    <?php
						if(is_array($ez_info['id'])):
							foreach($ez_info as $ez):
					?>
							<option value="<?php echo $ez['id']; ?>">Yes, want the EZ Option<br />(<?php echo $ez['num']; ?> Payments of $<?php echo $ez['cost']; ?>)</option>
                    <?php
							endforeach;
						elseif(is_array($ez_info)):
					?>
						<option value="<?php echo $ez_info['id']; ?>">Yes, I want the EZ Option (<?php echo $ez_info['num']; ?> Payments of $<?php echo $ez_info['cost']; ?>)</option>
					<?php endif; ?>

				</select>

				<div style="clear"></div>



            <a href="index.php">
				<button id="checkout" class="btn btn-danger" style="margin: auto; display: inline-block; margin-top: 25px; margin-bottom: 30px;"/>
					Let's go! <i class="fa fa-forward"></i>
		            </button>
            </a>

			</div>



</div>
<!-- END: Pop Up -->
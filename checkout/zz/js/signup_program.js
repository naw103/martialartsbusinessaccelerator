$(document).ready(function() {


	var total = $('span#order_total');
	//total.html($('tr.summit').attr('data-cost'));

	var guest_price = 149;

	// Default All Selects to First Option
  //	$('select').val($('select option:first-child').val());

	// Default Upsell DVD to YES
	$('select[name="upsell_dvd"]').val('1');

	// Default Country to USA
	$('select[name="country"]').val('1');


	$('input#btn_coupon').removeAttr('disabled');



	// If State DropDown is changed
	$('select[name="country"]').change(function()
	{
		var country_id = $(this).val();


		$.post(
				"ajax/get_states.php",
				{ country_id: country_id },
				function(xml)
				{
					var option = '<option value="">Please Select</option>';

					$(xml).find('response state').each(function()
					{
						var state_id = $(this).find('state_id').text();
						var state = $(this).find('state_name').text();

						option += '<option value="' + state_id + '">' + state + '</option>';

					});
					
					$('select[name="state"]').html(option);


				}
		);

	});

	// If close button is clicked
	$('div.close').click(function()
	{
		if($('#loader').is(':hidden'))
		{
			$('div#upsell_overlay').hide();
			$('table#upsell_container').hide();
		}

		$('div#cvv2_overlay').hide();
		$('#cvv2').hide();



	});

	$('select[name="hear_about"]').change(function(){

		var txt = $(this).val();

		if(txt == 'Other')
		{
			$('div.other_input, div.friend_input').hide();
			$('div.other_input').show();
		}
		else if(txt == 'Friend')
		{
			$('div.other_input, div.friend_input').hide();
			$('div.friend_input').show();
		}
		else
		{
			$('div.other_input, div.friend_input').hide();
		}

	});


	// DVD DropDown has been changed
	$('select[name="upsell_dvd"]').change(function()
	{

		var prod_cost = parseInt($('tr.summit').attr('data-cost'));

		var dvd_cost = parseInt($('tr.dvd').attr('data-cost'));

		var guest_cost = 0;


			if($('tr.guest').length > 0)
			{
				guest_cost = parseInt($('tr.guest').attr('data-cost'));
			}


		if($('select[name="upsell_dvd"]').val() == 1)
		{

			// Add Guest is checked



             if($('#billing_wrapper').hasClass('coupon_enabled_earlybird') && $('#form_wrapper').attr('data-payment') == '1')
             {
					    	total.html(prod_cost + dvd_cost + guest_cost - 100);
				        //	$('.total_coupon td span').html(total.html());
             }
			 else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html(198.5 + dvd_cost + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
              else if($('#billing_wrapper').hasClass('coupon_enabled_awesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 200) + dvd_cost + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
			  else if($('#billing_wrapper').hasClass('coupon_enabled_ryanisawesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 300) + dvd_cost + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
			 else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '3')
                    {
					    	total.html(prod_cost + dvd_cost + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
                    else
                    {
                      	total.html(prod_cost + dvd_cost + guest_cost);
                    }



			//$('.total_coupon td span').html(total.html());
			$('tr.dvd').show();


		}
		else
		{
			// Add DVD is not checked


            if($('#billing_wrapper').hasClass('coupon_enabled_earlybird') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html(prod_cost + guest_cost - 100);
				        //	$('.total_coupon td span').html(total.html());
                    }
            else if($('#billing_wrapper').hasClass('coupon_enabled_awesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 200) + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
			else if($('#billing_wrapper').hasClass('coupon_enabled_ryanisawesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 300) + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
			else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html(198.5 + guest_cost);
				        //	$('.total_coupon td span').html(total.html());
                    }
			else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '3')
                    {
					    	total.html(prod_cost + (guest_cost / 2));
				        //	$('.total_coupon td span').html(total.html());
                    }
                    else
                    {
                      	total.html(prod_cost + guest_cost);
                    }

			//$('.total_coupon td span').html(total.html());
			$('tr.dvd').hide();
		}

		couponApprove();

	});




	// FORM SUBMISSION
	$('input#checkout_now').click(function()
	{
		// check for incomplete form/errors
		validateForm($('form[name="checkout"]'));

		if($('form[name="checkout"]').find('input, select').hasClass('error'))
		{
			alert("Must Fill Out All Required Information");

		}
		else
		{
			// No Errors Found
			// Lets Proceed...
				var hear_about = $('select[name="hear_about"]').val();


				// Show POPUP, Lets Position it
				var pos = $(this).position();

				var left_pos = ($(window).width() / 2) - ($('#upsell_container').width() / 2);

				var top_pos = ($(window).height() / 2) - ($('#upsell_container').height() / 1.5)

				$('div#upsell_overlay').height($(document).height());

				$('div#upsell_overlay').show();

				$('#upsell_container').css({
												'left': left_pos + 'px',
												'top': top_pos + 'px',
												'position': 'fixed'
											});



				var prod_cost = parseInt($('tr.summit').attr('data-cost'));

				var dvd_cost = parseInt($('tr.dvd').attr('data-cost'));

				var guest_cost = 0;


				if($('tr.guest').length > 0)
				{
					guest_cost = parseInt($('tr.guest').attr('data-cost'));
				}


				// Check for DVD Upsell
				if($('select[name="upsell_dvd"]').val() == 1)
				{

                    if($('#billing_wrapper').hasClass('coupon_enabled_earlybird') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    total.html(prod_cost + dvd_cost + guest_cost - 100);
				        //	$('.total_coupon td span').html(total.html());
                    }
                    else if($('#billing_wrapper').hasClass('coupon_enabled_awesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 200) + dvd_cost + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
					else if($('#billing_wrapper').hasClass('coupon_enabled_ryanisawesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 300) + dvd_cost + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
					else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html(198.5 + dvd_cost + guest_cost);
				        //	$('.total_coupon td span').html(total.html());
                    }
					else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '3')
                    {
					    	total.html(prod_cost + dvd_cost + (guest_cost / 2));
				        //	$('.total_coupon td span').html(total.html());
                    }
                    else
                    {
                      total.html(prod_cost + dvd_cost + guest_cost);
                    }



                    $('tr.dvd').show();

				}
				else
				{
					// DVD Upsell Option is not selected


					//$('.total_coupon td span').html(total.html());
                     if($('#billing_wrapper').hasClass('coupon_enabled_earlybird') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    total.html(prod_cost + guest_cost - 100);
				        //	$('.total_coupon td span').html(total.html());
                    }
                    else if($('#billing_wrapper').hasClass('coupon_enabled_awesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 200) + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
					 else if($('#billing_wrapper').hasClass('coupon_enabled_ryanisawesome') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html((prod_cost - 300) + (guest_cost));
				        //	$('.total_coupon td span').html(total.html());
                    }
					else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '1')
                    {
					    	total.html(198.5 + dvd_cost + guest_cost);
				        //	$('.total_coupon td span').html(total.html());
                    }
					else if($('#billing_wrapper').hasClass('coupon_enabled_halfoff') && $('#form_wrapper').attr('data-payment') == '3')
                    {
					    	total.html(prod_cost + (guest_cost / 2));
				        //	$('.total_coupon td span').html(total.html());
                    }
                    else
                    {
                      total.html(prod_cost + guest_cost);
                    }



					$('tr.dvd').hide();
				}

				couponApprove();
				$('#upsell_container').fadeIn('fast');


		}


		return false;
	});


	$('#upsell_overlay').click(function(){
		if($('#loader').is(':hidden'))
		{
			$(this).hide();
			$('table#upsell_container').hide();
		}

	});

	$('#cvv2_overlay').click(function(){

		$(this).hide();
		$('#cvv2').hide();

	});




	// Checkout Upsell Container
	$('#upsell_container #checkout').click(function()
	{
		if($('#loader').is(':hidden'))
			{
			// Contact Info
			var firstname = $('input[name="firstname"]').val();
			var lastname = $('input[name="lastname"]').val();
			var address = $('input[name="address"]').val();
			var address2 = $('input[name="address2"]').val();
			var city = $('input[name="city"]').val();
			var zipcode = $('input[name="zipcode"]').val();
			var statecode = $('select[name="state"]').val();
			var countrycode = $('select[name="country"]').val();
			var phone = $('input[name="phone"]').val();
			var email = $('input[name="email"]').val();
			var shirt_size = $('select[name="tshirt"]').val();

			var coupon_code = $('input[name="coupon"]').val();
			var hear_about = $('select[name="hear_about"]').val();

			if(hear_about == 'Friend')
			{
				hear_about = 'Friend:  ' + $('input[name="hear_friend"]').val();
			}
			else if(hear_about == 'Other')
			{
				hear_about = 'Other:  ' + $('input[name="hear_other"]').val();
			}


			// Guest Info
			var guest = 0;
			var guest_firstname = '';
			var guest_lastname = '';
			var guest_tshirt = '';

			if($('input[name="guest_firstname"]').length > 0)
			{
				guest = 1;
				guest_firstname = $('input[name="guest_firstname"]').val();
				guest_lastname = $('input[name="guest_lastname"]').val();
				guest_tshirt = $('select[name="guest_tshirt"]').val();
			}

			// Credit Card Info
			var cc_name = $('input[name="cc_name"]').val();
			var cc_num = $('input[name="cc_num"]').val();
			var cc_type = '';

			$('ul.cards').find('li').each(function()
			{
				if(!$(this).hasClass('off'))
				{
					cc_type = $(this).attr('class');
				}

			});

			var cc_exp_month = $('select[name="cc_exp_month"]').val();
			var cc_exp_year = $('select[name="cc_exp_year"]').val();
			var cc_code = $('input[name="cvv2"]').val();

			// DVD Option
			var dvd = $('select[name="upsell_dvd"]').val();

			var ez_option = $('div#form_wrapper').attr('data-payment');

			$('div#loader').fadeIn('fast');


			$.post(
					'program/process_form.php',
					{
						firstname: firstname,
						lastname: lastname,
						address: address,
						address2: address2,
						city: city,
						zipcode: zipcode,
						statecode: statecode,
						countrycode: countrycode,
						phone: phone,
						email: email,
						shirt_size: shirt_size,

						guest: guest,
						guest_firstname: guest_firstname,
						guest_lastname: guest_lastname,
						guest_shirt_size: guest_tshirt,

						cc_name: cc_name,
						cc_num: cc_num,
						cc_type: cc_type,
						cc_exp_month: cc_exp_month,
						cc_exp_year: cc_exp_year,
						cc_code: cc_code,

						dvd: dvd,
						ez_option: ez_option,
                        coupon_code: coupon_code,

						hear_about: hear_about
					},
					function(response)
					{

						if(response == 'APPROVED')
						{

							$('div#loader').fadeOut('fast');
							window.location.href = 'thank-you.php';
						}
						else
						{
							$('div#loader').fadeOut('fast');
							alert(response);
						}

					}

				);

		}

	});


	// FORM VALIDATION
	function validateForm(form)
	{
	   form.find('input.required, select.required').each(function()
	   {
			if($(this).val() == '')
			{
				$(this).addClass('error');
			    $(this).removeClass('valid');

				if($(this).attr('title') != '' && $(this).next('div.error_msg').length == 0)
				{
					$('<div class="error_msg">' + $(this).attr('title') + '</div>').insertAfter($(this));
				}

			}
			else
			{
			   if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				var str = $('input[name="email"]').val();

				var check_at = str.indexOf('@');

				var check_dot = str.indexOf('.');

				if(check_at != -1 && check_dot != -1)
				{
					$(this).removeClass('error');
					$(this).addClass('valid');
			   		$(this).next('div.error_msg').remove();
				}
				else
				{
					$(this).removeClass('valid');
					$(this).addClass('error');

					if($(this).next('div.error_msg').length == 0)
					{
						$('<div class="error_msg">Invalid Email</div>').insertAfter($(this));
					}
					else
					{
						$(this).next('div.error_msg').html('<div class="error_msg">Invalid Email</div>');
					}
				}
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
			}
	   });

	}


	$('input.required').keyup(function()
	{
		if($(this).val() != '')
		{
			if($(this).attr('name') == 'email')
			{
				// Email field has data in it
				// Validate to make sure its a valid email format
				var str = $('input[name="email"]').val();

				var check_at = str.indexOf('@');

				var check_dot = str.indexOf('.');

				if(check_at != -1 && check_dot != -1)
				{
					$(this).removeClass('error');
					$(this).addClass('valid');
			   		$(this).next('div.error_msg').remove();
				}
				else
				{
					$(this).removeClass('valid');
					$(this).addClass('error');

					if($(this).next('div.error_msg').length == 0)
					{
						$('<div class="error_msg">Invalid Email</div>').insertAfter($(this));
					}
					else
					{
						$(this).next('div.error_msg').html('<div class="error_msg">Invalid Email</div>');
					}
				}
			}
			else
			{
				$(this).removeClass('error');
				$(this).addClass('valid');

			   $(this).next('div.error_msg').remove();

		   }
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');

			if($(this).attr('title') != '' && $(this).next('div.error_msg').length == 0)
				{
					$('<div class="error_msg">' + $(this).attr('title') + '</div>').insertAfter($(this));
				}


		}

	});

	$('input[name="address2"]').keyup(function()
	{
		if($(this).val() != '')
		{
			$(this).addClass('valid');
		}
		else
		{
			$(this).removeClass('valid');
		}

	});




	$('select.required').change(function()
	{
		if($(this).val() != '')
		{
			$(this).removeClass('error');
			$(this).addClass('valid');

			$(this).next('div.error_msg').remove();
		}
		else
		{
			$(this).removeClass('valid');
			$(this).addClass('error');

			if($(this).attr('title') != '' && $(this).next('div.error_msg').length == 0)
				{
					$('<div class="error_msg">' + $(this).attr('title') + '</div>').insertAfter($(this));
				}
		}

	});


	$('span#cvv2_define').click(function() {



		var left_pos = ($(window).width() / 2) - ($('#cvv2').width() / 2);

			var top_pos = ($(window).height() / 2) - ($('#cvv2').height() / 2)

			$('#cvv2').css({
											'left': left_pos + 'px',
											'top': top_pos + 'px',
											'position': 'fixed'
										});

		$('#cvv2').show();


		$('div#cvv2_overlay').height($(document).height());
		$('div#cvv2_overlay').show();

	});


	$('input[name="firstname"], input[name="lastname"]').blur(function(){
		$('input[name="cc_name"]').val($('input[name="firstname"]').val() + ' ' + $('input[name="lastname"]').val());
	});



	$('input#btn_coupon').click(function(){
		var coupon_code = $('input[name="coupon"]').val();

		$.post(
					'ajax/coupon_check.php',
					{
						coupon_code: coupon_code
					},
					function(response)
					{

						if(response == '1')
						{
                            // mm

							$('#billing_wrapper').html('');
							$('.total_coupon').show();
							total.html('0.00');

							$('#billing_wrapper').html('');
							$('input#btn_coupon').attr('disabled','disabled');
							$('#billing_wrapper').addClass('coupon_enabled');
						}

						if(response == '2')
						{
                            // eb

						   // subscription payment
						   if($('#form_wrapper').attr('data-payment') == '3')
						   {
								$('.total_coupon').find('td').eq(1).html('');
								$('.total_coupon').show();
								//total.html(total.html());
								$('tr.summit').attr('data-cost', 149);
								$('tr.summit').find('td').eq(1).html('$149');
								$('tr.summit span').html('(2 Monthly Payments of $149)');

                                $('#upsell_container .total_coupon').find('td').eq(1).html('');
								$('#upsell_container .total_coupon').show();
								//total.html(total.html());
								$('#upsell_container tr.summit').attr('data-cost', 149);
								$('#upsell_container tr.summit').find('td').eq(1).html('$149');
								$('#upsell_container tr.summit span').html('(2 Monthly Payments of $149)');

								var tmp = 149;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = 149 + 149;
                                }

								total.html(tmp);
						   }
						   else
						   {
                                var tmp = 199;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = 199 + 149;
                                }

						        // pay in full
						   		$('.total_coupon').find('td').eq(1).find('span').html(100);
								$('.total_coupon').show();
								total.html(total.html() - 100);

                                $('#upsell_container .total_coupon').find('td').eq(1).find('span').html(100);
								$('#upsell_container .total_coupon').show();
							//$('#billing_wrapper').html('');

							}

							$('input#btn_coupon').attr('disabled','disabled');
							$('#billing_wrapper').addClass('coupon_enabled_earlybird');
						}

						if(response == '3')
						{
                            // eb

						   // subscription payment
						   if($('#form_wrapper').attr('data-payment') == '3')
						   {
						   		var tmp = 99.25;

								var coupon_disc = 0;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = tmp + (149 / 2);;
								  coupon_disc = coupon_disc + (149 / 2);
                                }

								if($('tr.dvd').is(':visible'))
                                {
                                  tmp = tmp + 69;
                                }


								$('.total_coupon').find('td').eq(1).html(coupon_disc);
								$('.total_coupon').show();
								//total.html(total.html());
								$('tr.summit').attr('data-cost', 99.25);
								$('tr.summit').find('td').eq(1).html('$99.25');
								$('tr.summit span').html('(2 Monthly Payments of $99.25)');

                                $('#upsell_container .total_coupon').find('td').eq(1).html(coupon_disc);
								$('#upsell_container .total_coupon').show();
								//total.html(total.html());
								$('#upsell_container tr.summit').attr('data-cost', 99.25);
								$('#upsell_container tr.summit').find('td').eq(1).html('$99.25');
								$('#upsell_container tr.summit span').html('(2 Monthly Payments of $99.25)');



								total.html(tmp);
						   }
						   else
						   {
                                var tmp = 198.5;
								var coupon_disc = $('tr.summit').attr('data-cost') / 2;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = tmp + 149 ;
								  coupon_disc = coupon_disc + 149;
                                }

								if($('tr.dvd').is(':visible'))
                                {
                                  tmp = tmp + 69;
                                }




						        // pay in full
						   		$('.total_coupon').find('td').eq(1).find('span').html(coupon_disc);
								$('.total_coupon').show();
								total.html(tmp);

                                $('#upsell_container .total_coupon').find('td').eq(1).find('span').html(coupon_disc);
								$('#upsell_container .total_coupon').show();
							//$('#billing_wrapper').html('');

							}

							$('input#btn_coupon').attr('disabled','disabled');
							$('#billing_wrapper').addClass('coupon_enabled_halfoff');
						}


						if(response == '4')
						{
                            // brewster

						   // subscription payment
						   if($('#form_wrapper').attr('data-payment') == '3')
						   {
								$('.total_coupon').find('td').eq(1).html('');
								$('.total_coupon').show();
								//total.html(total.html());
								$('tr.summit').attr('data-cost', 37.25);
								$('tr.summit').find('td').eq(1).html('$37.25');
								$('tr.summit span').html('(2 Monthly Payments of $37.25)');

                                $('#upsell_container .total_coupon').find('td').eq(1).html('');
								$('#upsell_container .total_coupon').show();
								//total.html(total.html());
								$('#upsell_container tr.summit').attr('data-cost', 37.25);
								$('#upsell_container tr.summit').find('td').eq(1).html('$37.25');
								$('#upsell_container tr.summit span').html('(2 Monthly Payments of $37.25)');

								var tmp = 37.25;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = tmp + 149;
                                }

								if($('tr.dvd').is(':visible'))
                                {
                                  tmp = tmp + 69;
                                }

								total.html(tmp);
						   }
						   else
						   {
                                var tmp = 74.50;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = tmp + 149;
                                }

								if($('tr.dvd').is(':visible'))
                                {
                                  tmp = tmp + 69;
                                }

						        // pay in full
						   		$('.total_coupon').find('td').eq(1).html('');
								$('.total_coupon').show();


								$('tr.summit').attr('data-cost', 74.50);
								$('tr.summit').find('td').eq(1).html('$74.50');

								$('#upsell_container tr.summit').attr('data-cost', 74.50);
								$('#upsell_container tr.summit').find('td').eq(1).html('$74.50');

                                $('#upsell_container .total_coupon').find('td').eq(1).html('');
								$('#upsell_container .total_coupon').show();

								total.html(tmp);

							//$('#billing_wrapper').html('');

							}

							$('input#btn_coupon').attr('disabled','disabled');
							$('#billing_wrapper').addClass('coupon_enabled_brewster');
						}

                        if(response == '5')
						{
                            // eb

						   // subscription payment
						   if($('#form_wrapper').attr('data-payment') == '3')
						   {
								$('.total_coupon').find('td').eq(1).html('');
								$('.total_coupon').show();
								//total.html(total.html());
								$('tr.summit').attr('data-cost', 149);
								$('tr.summit').find('td').eq(1).html('$149');
								$('tr.summit span').html('(2 Monthly Payments of $149)');

                                $('#upsell_container .total_coupon').find('td').eq(1).html('');
								$('#upsell_container .total_coupon').show();
								//total.html(total.html());
								$('#upsell_container tr.summit').attr('data-cost', 149);
								$('#upsell_container tr.summit').find('td').eq(1).html('$149');
								$('#upsell_container tr.summit span').html('(2 Monthly Payments of $149)');

								var tmp = 149;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = 149 + 149;
                                }

								total.html(tmp);
						   }
						   else
						   {
                                var tmp = 199;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = 199 + 149;
                                }

						        // pay in full
						   		$('.total_coupon').find('td').eq(1).find('span').html(200);
								$('.total_coupon').show();
								total.html(total.html() - 200);

                                $('#upsell_container .total_coupon').find('td').eq(1).find('span').html(200);
								$('#upsell_container .total_coupon').show();
							//$('#billing_wrapper').html('');

							}

							$('input#btn_coupon').attr('disabled','disabled');
							$('#billing_wrapper').addClass('coupon_enabled_awesome');
						}

						if(response == '6')
						{

							 var tmp = 199;

                                if($('tr.guest').is(':visible'))
                                {
                                  tmp = 199 + 149;
                                }

						        // pay in full
						   		$('.total_coupon').find('td').eq(1).find('span').html(300);
								$('.total_coupon').show();
								total.html(total.html() - 300);

                                $('#upsell_container .total_coupon').find('td').eq(1).find('span').html(300);
								$('#upsell_container .total_coupon').show();
							//$('#billing_wrapper').html('');



							$('input#btn_coupon').attr('disabled','disabled');
							$('#billing_wrapper').addClass('coupon_enabled_ryanisawesome');

						}

						if(response == '0')
						{
							alert("Invalid  Coupon");
						}

					}



				);

	   //couponApprove();

	});

	function couponApprove()
	{

		if($('#billing_wrapper').hasClass('coupon_enabled'))
		{
			$('#billing_wrapper').html('');
			$('.total_coupon').show();
			total.html('0.00');
		}

        if($('#billing_wrapper').hasClass('coupon_enabled_earlybird'))
        {


        }
	}



});


<div class="clear"></div>
<footer>
		<p>
			Any questions or issues, email <a href="mailto:info@martialartsbusinesssummit.com">info@martialartsbusinesssummit.com</a>
		</p>

		<p class="privacy">&copy; <a href="http://www.fconlinemarketing.com">FC Online Marketing Inc.</a> <?php echo date("Y"); ?> All Rights Reserved</p>

</footer>

<script src="https://www.ilovekickboxing.com/intl_js/cufon.js"></script>
<script src="js/fonts/Futura_Medium_500.font.js"></script>
<script src="js/fonts/Futura_Std_650.font.js"></script>
<script src="js/fonts/Futura_Std_oblique_500.font.js"></script>
<script src="js/fonts/Futura-CondensedMedium_500.font.js"></script>
<script src="js/fonts/Futura_Std_Bold_Condensed.font.js"></script>
<script src="js/cufon_fonts.js"></script>
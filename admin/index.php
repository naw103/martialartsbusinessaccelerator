<?php
	require('program/program.php');

	date_default_timezone_set('America/New_York');

	$todays_date = date('Y-m-d');


?>
<!DOCTYPE html>
<html>
<head>
	<title>Martial Arts Business Summitt Admin</title>

	<meta name="viewport" content="intial-scale=1.0" />

	<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css?ver=1.0" />
	<link rel="stylesheet" href="../css/pages.css?ver=1.0" />
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/admin.css?ver=1.0" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


</head>
<body>

<!-- BEGIN: Page Content -->
<div id="container">
	<div id="page_content">


		<div id="opening_content"></div>

		<br /><br />

		<div id="featured_numbers">


			<!-- ticket sales -->
			<div id="ticket_sales" class="featured">
				<span class="head">Total</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo sizeof($orders);  ?></span>
			</div>



		</div>


		<h2>All Orders</h2>
		<div style="clear: both;"></div>
		<div style="border: 1px solid #CCC; display: inline-block; padding: 15px; float: left;">
			<div style="background: #69b97c; width: 100px; height: 20px; float: left;"></div> <span style="float: left;">Continuing MABA</span>
		</div>

		<div style="clear: both;"></div>

		<br /><br />

		<table class="main" style="width: 100%; margin-top: 50px;">
			<thead>
				<tr class="head">
					<td>Order Id</td>
					<td>Name</td>
					<td>Phone</td>
					<td>Email</td>
					<td>Payment Option</td>
					<td>Total</td>
					<td>
						Order<br />
						Date
					</td>
					<td>Added to Facebook</td>

				</tr>
			</thead>

			<tbody>

	   			<?php foreach($orders as $order): ?>

				<?php
					$jt_class = '';
					if($order['maba_upgrade'] == '1'):
						$jt_class = 'background: #69b97c;';
					endif;

				?>

				<tr style="<?php echo $jt_class; ?>" data-orderid="<?php echo $order['order_id']; ?>">
					<td><?php echo $order['order_id']; ?></td>
					<td><?php echo ucwords($order['firstname']) . ' ' . ucwords($order['lastname']); ?></td>
					<td><?php echo $order['phone']; ?></td>
					<td><?php echo $order['email']; ?></td>

					<td>
							<?php
								if($order['subscription_type_id'] == '1'):
									echo 'PIF';
								else:
									echo 'Monthly';
								endif;
							?>
					</td>

					<td>
						<?php
								if($order['subscription_type_id'] == '1'):
									echo '$1,599.00';
								else:
									echo '$1,791.00';
								endif;
						?>
					</td>



					<td><?php echo date('m/d/y', strtotime($order['order_date'])); ?></td>
					<td><input type="checkbox" value="" name="added_facebook" class="added_facebook"  <?php echo $order['added_facebook'] == '1' ? 'checked="true"': ''; ?>/></td>


				</tr>

				<?php endforeach; ?>
		</tbody>

		</table>



	</div>
</div>


<script src="js/jquery.dataTables.js"></script>


<script>
	$(document).ready(function() {
	    oTable = $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});



		$('table.main tbody').on('click', 'input.added_facebook', function () {

			status = '0';
			if($(this).is(':checked'))
			{
				status = '1';
			}

			var orderid = $(this).parents('tr').attr('data-orderid');

			$.ajax({
					url: 'ajax/facebook.php',
					data: {
								status: status,
								orderid: orderid
					},
					type: 'POST'

			});

    	} );




	} );

	$(window).resize(function(){

		 jQuery(oTable).dataTable().fnDestroy();
         oTable = $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});

		var width = $(window).width();

		if(width > 1000)
		{
			oTable.width(1000);
		}
		else
		{
			o.Table.width(width);
		}
	});




</script>

</body>
</html>
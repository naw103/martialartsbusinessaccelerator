<?php
    require('../program/class.database.php');
	require('../program/functions.php');

	$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();


// mastermind order details
$sql = "SELECT co.customer_id, cc.firstname, cc.lastname, co.cc_name
		FROM checkout_orders co
			LEFT JOIN checkout_friends cf ON co.order_id = cf.order_id
			LEFT JOIN checkout_products_join cpj ON co.order_id = cpj.order_id AND cpj.product_id = 2,
			checkout_customers cc
		WHERE co.customer_id = cc.customer_id AND order_product_id LIKE 'MABSMASTERMIND%'
		ORDER BY co.order_date DESC";

$mastermind = $db->Execute($sql);


    $db->close();

  	# Build XML
	header('Content-type: text/xml');
	header('Cache-Control: no-cache');
	echo "<?xml version=\"1.0\" ?>\n";
	echo "<response>\n";

		for($i = 0; $i < sizeof($mastermind); $i++):
			//echo "\t<masterminder>" . $mastermind[$i]['firstname'] . ' ' . $mastermind[$i]['lastname'] . "</masterminder>\n";
			
			echo "\t<masterminder>" . $mastermind[$i]['cc_name'] . "</masterminder>\n";
		endfor;



	echo "</response>";

?>
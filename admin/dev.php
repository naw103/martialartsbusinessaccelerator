<?php
	require('program/program.php');

	date_default_timezone_set('America/New_York');

	$todays_date = date('Y-m-d');


?>
<!DOCTYPE html>
<html>
<head>
	<title>Martial Arts Business Summitt Admin</title>

	<meta name="viewport" content="intial-scale=1.0" />

	<link rel="stylesheet" href="https://www.ilovekickboxing.com/intl_css/reset.css?ver=1.0" />
	<link rel="stylesheet" href="../css/pages.css?ver=1.0" />
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/admin.css?ver=1.0" />

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>


</head>
<body>

<!-- BEGIN: Page Content -->
<div id="container">
	<div id="page_content">

		<?php include('header.php'); ?>

		<div id="opening_content"></div>

		<br /><br />

		<div id="featured_numbers">

			 <!-- ticket sales -->
			<div id="ticket_sales" class="featured">
				<span class="head">Current Total</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($maba_collected + $sub_total, 2); ?></span>
				<span style="font-size: 18px; display: block;"></span>
			</div>

			<!-- ticket sales -->
			<div id="ticket_sales" class="featured">
				<span class="head">Final Total</span>
				<span class="num" style="display: block; padding-top: 8px; padding-bottom: 8px;"><?php echo '$' . number_format($mabs15_totals, 2); ?></span>
				<span style="font-size: 18px; display: block;">(<?php echo sizeof($mabs15); ?>)</span>
			</div>




			<!-- # orders today -->
			<div id="todays_orders" class="featured">
				<span class="head">Today's Orders</span><br />
				<span class="num"><?php echo sizeof($orders_today); ?></span>
			</div>

		</div>


		<h2>All Orders</h2>

		<table class="main" style="width: 100%;">
			<thead>
				<tr class="head">
					<td>Order Id</td>
					<td>Name</td>
					<td>Product<br />Code</td>
					<td>Products<br />Purchased</td>
					<td>Total</td>

					<td>
						Order<br />
						Date
					</td>
					<td>Payments</td>
				</tr>
			</thead>

			<tbody>

	   			<?php foreach($orders as $order): ?>

				<?php
					if(isset($suspended_subscriptions)):
						foreach($suspended_subscriptions as $suspended):

							$jt_class = '';
							if($order['order_id'] == $suspended['order_id']):
								$jt_class = 'color: red;';
								break;
							endif;
						endforeach;
					endif;
				?>

				<tr style="<?php echo $jt_class; ?>">
					<td><?php echo $order['order_id']; ?></td>
					<td><?php echo ucwords($order['firstname']) . ' ' . ucwords($order['lastname']); ?></td>
					<td class="code">
						<?php if(strpos($order['order_product_id'], 'MABA') !== false): ?>
						MABA
						<?php else: ?>
						MABSDVD
						<?php endif; ?>

					</td>

					<td>
						<span class="fa fa-bolt"></span>
					</td>

					<td>
						<?php if($order['subscription_type_id'] != 1): ?>

							<?php
							   	foreach($ezpay as $option):
									if($order['ez_pay'] == $option['id']):
											echo '$' . number_format(round(($order['order_total'] * $option['num'])), 2);
										break;
									endif;
								endforeach;

							?>

						<?php else: ?>
						<?php echo '$' . number_format($order['paid_ticket_cost'], 2); ?>
						<?php endif; ?>
					</td>

					<td><?php echo date('m/d/y', strtotime($order['order_date'])); ?></td>
					<td>
						<?php if($order['subscription_type_id'] == 1): ?>
						1/1
						<?php else: ?>

						<?php
								$jt = 1;
								foreach($subscription_payments as $payment):
									//echo $order['order_id'] . ' => ' . $payment['order_id'] . '<br />';
									if($order['order_id'] == $payment['order_id']):
										$jt++;
									endif;
								endforeach;
						?>

						<span><?php echo $jt; ?>/9</span>

						<?php endif; ?>
					</td>

				</tr>

				<?php endforeach; ?>
		</tbody>

		</table>


		<?php if(isset($suspended_subscriptions)): ?>
		<div style="margin-top: 100px; border-top: 1px solid #CCC;">
			<h2 style="text-align: center; padding-top: 14px;">Suspended Subscriptions</h2>

			<table>
				<thead>
					<tr>
						<!--<th style="text-align: center;">Order ID</th>-->
						<th style="text-align: center;">Name</th>
						<th style="text-align: center;">Email</th>
						<th style="text-align: center;">Phone</th>
						<th>State</th>
						<!--<th style="text-align: center;">Date Suspended</th>-->
						<th style="">Payment Link</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($suspended_subscriptions as $suspended):
					   /*	$suspended['phone'] = str_replace('-', '', $suspended['phone']);
						$suspended['phone'] = str_replace('.', '', $suspended['phone']);
						$suspended['phone'] = str_replace(')', '', $suspended['phone']);
						$suspended['phone'] = str_replace('(', '', $suspended['phone']);
						$phone = substr($suspended['phone'], 0, 3) . '-' . substr($suspended['phone'], 3, 3) . '-' . substr($suspended['phone'], 6, 4);
						*/
					?>
					<tr>
						<!--<td><?php echo $suspended['order_id']; ?></td>-->
						<td><?php echo ucwords(strtolower($suspended['firstname'])) . ' ' . ucwords(strtolower($suspended['lastname'])); ?></td>
						<td style="text-align: left; padding-left: 10px;"><?php echo '<a href="mailto:' . strtolower(trim($suspended['email'])) . '>">' . strtolower(trim($suspended['email'])) . '</a>'; ?></td>
						<td><?php echo $suspended['phone']; ?></td>
						<td><?php echo $suspended['Code']; ?></td>
						<!--<td><?php echo date('m/d/y', strtotime($suspended['date_suspended'])); ?></td>-->
						<td><a target="_blank" href="https://www.fconlinemarketing.com/billing/order/update/<?php echo $suspended['session_key']; ?>">View</a>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<?php endif; ?>




	</div>
</div>


<script src="js/jquery.dataTables.js"></script>


<script>
	$(document).ready(function() {
	    oTable = $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});




	} );

	$(window).resize(function(){

		 jQuery(oTable).dataTable().fnDestroy();
         oTable = $('table.main').dataTable({

			    "aaSorting": [[ 0, "desc" ]],
				"aoColumnDefs": [ { "sClass": "hidden", "aTargets": [ 0 ] } ],
				"iDisplayLength": 25,
				"aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]]

		});

		var width = $(window).width();

		if(width > 1000)
		{
			oTable.width(1000);
		}
		else
		{
			o.Table.width(width);
		}
	});




</script>

</body>
</html>
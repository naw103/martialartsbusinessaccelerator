<?php



$to      = $email;
//$to = 'james@fconlinemarketing.com';

$subject = "Thank You, for MABS 2014.";

$message = "Hey," . "\n\n";

$message .= "Mike Parrella here. I just wanted to\n"
		 .  "swing by and say a big THANK YOU\n"
		 .  "for MABS 2014." . "\n\n";

$message .= "You probably didn't know this, but\n"
		 .  "hosting an event like this has been\n"
		 .  "a long time dream of mine ;-)"  . "\n\n";

$message .= "I'm so glad you came and were a\n"
		 .  "part of it." . "\n\n";

$message .= "And I hope you put what you learned\n"
		 .  "to good use. Remember: This week,\n"
		 .  "just focus on your top 7 goals." . "\n\n";

$message .= "Forget everything else for now. Just\n"
		  . "take care of 7 action-steps, and\n"
		  . "then next week you can do 7 more."  . "\n\n";

$message .= "Soon enough, you'll be hitting big\n"
	     .  "numbers and more family time just\n"
		 .  "like me and the masterminders :-)" . "\n\n";

$message .= "Also, we are extending the deadlines\n"
		 .  "on our special MABS offers to this\n"
		 .  "FRIDAY AT MIDNIGHT." . "\n\n";

$message .= "If you want to grab these school-\n"
		 .  "growing products & services, here's\n"
		 .  "your chance:" . "\n\n";

$message .= '///////' . "\n\n";

$message .= '- Website:' . "\n\n";

$message .= 'Down Payment: $99 (Regular $599)' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1ojaAkr' . "\n\n";

$message .= '- Use this coupon code:' . "\n";

$message .= 'MABSWEBSITE' . "\n\n";

$message .= '///////' . "\n\n";

$message .= '- Email Machine:' . "\n\n";

$message .= 'Set-Up Fee: $49 (Regular $99)' . "\n"
		 .  'Monthly: $79 (Regular $99)' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1jRhdZb' . "\n\n";

$message .= '///////' . "\n\n";

$message .= '- MA Power Listings:' . "\n\n";

$message .= 'Set-Up Fee: $49 (Regular $99)' . "\n"
		 .	'Monthly: $79 (Regular $99)' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1jt0f0A' . "\n\n";

$message .= '- Coupon Code:' . "\n";

$message .= 'MABSPOWERLISTING' . "\n\n";

$message .= '///////' . "\n\n";

$message .= '- MA Pro Newsletter:' . "\n\n";

$message .= 'Just $1 Down for 30 days' . "\n\n";

$message .= '- Use this link:' . "\n";

$message .= 'http://bit.ly/1liL6yS' . "\n\n";

$message .= '///////' . "\n\n";

$message .= 'REMEMBER: THESE EXPIRE FRIDAY' . "\n"
		 .  'AT MIDNIGHT (PST).' . "\n\n";

$message .= "Grab'em now, and let's grow" . "\n"
		 .  "your school like never before." . "\n\n";

$message .= "See you next year," . "\n"
		 .  "at MABS 2015!!" . "\n\n";

$message .= 'MP' . "\n\n\n\n";


//$headers = 'From: info@martialartsbusinesssummit.com';
$headers = 'From: "FC Online Marketing" <info@fconlinemarketing.com>' . "\r\n" .
        	'Reply-To: info@fconlinemarketing.com' . "\r\n" .
			'BCC: printsupport@fconlinemarketing.com' . "\r\n" .
        	'X-Mailer: PHP/' . phpversion();

if(mail($to, $subject, $message, $headers)):

  $mail_result = 1;

else:
  $mail_result = 0;
endif;

?>
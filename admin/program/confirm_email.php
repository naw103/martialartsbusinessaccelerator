<?php



$to      = $email;
//$to = 'james@fconlinemarketing.com';

$subject = "You ready!? (MABS Confirmation Email).";

$message = "Hey" . "\n\n";

$message .= 'MABS 2014 is just around the corner. ' . "\n"
		 .  'In fact, it\'s just 10 days away to be ' . "\n"
		 .  'exact ;-).' . "\n\n";

$message .= 'I just wanted to swing by, make sure ' . "\n"
		 .  'you\'re all ready, and send you a ' . "\n"
		 .  '"refresher" on the logistics.'  . "\n\n";

$message .= 'We\'re all super pumped for this and I ' . "\n"
		 .  'can\'t wait to see you there. This thing ' . "\n"
		 .  'is going to be huge. I know you\'ll ' . "\n"
		 .  'love it.' . "\n\n";

$message .= 'Alright, so here\'s that "refresher":' . "\n\n";

$message .= '- The conference "officially" starts Fri., ' . "\n"
		 .  'March 28, but you can check in and ' . "\n"
		 .  'get your badge on Thursday evening ' . "\n"
		 .  'from 7-10pm.'  . "\n\n";

$message .= '(I recommend doing this so you can ' . "\n"
		 .  'beat the rush Friday morning.)' . "\n\n";

$message .= '- We\'ve got you covered for breakfast. ' . "\n"
		 .  'We\'re providing a continental set-up ' . "\n"
		 .  'with bagels and shmear, yogurt, fruit, ' . "\n"
		 .  'etc.' . "\n\n";

$message .= '- The conference takes place in NYC ' . "\n"
		 .  'at the LaGuardia airport Marriott.' . "\n\n";

$message .= 'If there are any other details you\'d like ' . "\n"
		 .  'reminders on, check out the official ' . "\n"
		 .  'MABS 2014 website here:' . "\n\n";

$message .= 'http://www.martialartsbusinesssummit2014.com'  . "\n\n";

$message .= 'Or, give my office a call at: ' . "\n"
		 .  '(516) 543-0041.' . "\n\n";

$message .= 'Talk soon!' . "\n"
		 .	'Mike Parrella' . "\n\n";


//$headers = 'From: info@martialartsbusinesssummit.com';
$headers = 'From: "info@fconlinemarketing.com" <info@fconlinemarketing.com>' . "\r\n" .
        	'Reply-To: info@fconlinemarketing.com' . "\r\n" .
			'BCC: printsupport@fconlinemarketing.com' . "\r\n" .
        	'X-Mailer: PHP/' . phpversion();

if(mail($to, $subject, $message, $headers)):

  $mail_result = 1;

else:
  $mail_result = 0;
endif;

?>
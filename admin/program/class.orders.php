<?php

class Orders extends database
{
	public function all_orders()
	{
		# This method will return all orders
		$data = array();


		$sql = "SELECT
							cc.*,
							co.*
				FROM
							checkout_customers cc,
							checkout_orders co
				WHERE
							cc.customer_id = co.customer_id AND
                            co.status = 'PAID' AND
							(co.order_product_id LIKE 'MABA-%') AND
							co.order_id >= 1571";

		$data = $this->Execute($sql);


		return $data;
	}

	function subscription_payments()
	{
		# This method will return the payments from the subscriptiosn table,
		# so we can later compare and see how many payments have been made per order.
		$data = array();

		$sql = "SELECT
						*
				FROM
						checkout_subscription cs
				WHERE
						cs.order_id <> 0";

		$data = $this->Execute($sql);
		//echo '<pre>';
		//print_r($data);
		//echo '</pre>'; die;
		return $data;
	}



	function suspended_subscriptions()
	{
		# This method will return all suspended subscriptions
		$data = array();

		$sql = "SELECT
						css.*,
						cc.firstname,
						cc.lastname,
						cc.email,
						cc.phone,
						st.Code,
						css.session_key
				FROM
						checkout_suspended_subscription css,
						checkout_orders co,
						checkout_customers cc,
						States st
				WHERE
                        st.ID = cc.state_id AND
						co.order_product_id LIKE 'MABA-%' AND
						css.order_id = co.order_id AND
						cc.customer_id = co.customer_id AND
						css.status = '0' AND
						css.show = '1'
				ORDER BY
						cc.firstname,
						cc.lastname";

		$data = $this->Execute($sql);

		return $data;
	}

	public function orders_today()
	{
		# This method will return all orders
		$data = array();

		date_default_timezone_set("America/New_York");
		$today = date('Y-m-d');
		$tomorrow = date('Y-m-d', strtotime($today . "+ 1 day"));


		$sql = "SELECT
							*
				FROM
							checkout_customers cc,
							checkout_orders co
				WHERE
							co.coupon_code <> 'FREEACCESS' AND
							cc.customer_id = co.customer_id AND
							(co.order_product_id LIKE 'MABA-%') AND
							DATE(co.order_date) >= DATE('$today') AND DATE(co.order_date) < DATE('$tomorrow')";

		$data = $this->Execute($sql);


		return $data;
	}

	public function mabs_dvd()
	{
		# This method will return all orders
		$data = array();


		$sql = "SELECT
							*
				FROM
							checkout_customers cc,
							checkout_orders co
				WHERE
							co.order_total <> '0.00' AND
							cc.customer_id = co.customer_id AND
							co.order_product_id LIKE 'MABSDVD-%' AND
							co.order_id > 452";

		//$data = $this->Execute($sql);


		return $data;
	}

	public function mabs15_friends()
	{
		# This method will return all orders
		$data = array();


		$sql = "SELECT
							*
				FROM
							checkout_customers cc,
							checkout_orders co,
							checkout_products_join cpj
				WHERE
							cpj.product_id = 3 AND
							co.order_id = cpj.order_id AND
							cc.customer_id = co.customer_id AND
							co.order_product_id LIKE 'MABS15-%' AND
							co.order_id > 452";

		//$data = $this->Execute($sql);


		return $data;
	}

	public function mabs15_dvd()
	{
		# This method will return all orders
		$data = array();


		$sql = "SELECT
							*
				FROM
							checkout_customers cc,
							checkout_orders co,
							checkout_products_join cpj
				WHERE
							cpj.product_id = 11 AND
							co.order_id = cpj.order_id AND
							cc.customer_id = co.customer_id AND
							co.order_product_id LIKE 'MABS15-%' AND
							co.order_id > 452";

		//$data = $this->Execute($sql);


		return $data;
	}

	public function mabs15_totals($orders)
	{
		$data = array();
		$total = 0;

		$ezpay = $this->get_ezpay();


		foreach($orders as $order):

			if($order['subscription_type_id'] != 1):

			   	foreach($ezpay as $option):
					if($order['ez_pay'] == $option['id']):
							$total += ($order['order_total'] * $option['num']);
						break;
					endif;
				endforeach;

			else:

			$total += $order['order_total'];

			endif;

		endforeach;




		return $total;
	}

	public function maba_collected($orders)
	{
		$data = array();
		$total = 0;

		$ezpay = $this->get_ezpay();


		foreach($orders as $order):

			if($order['subscription_type_id'] != 1):

			   	foreach($ezpay as $option):
					if($order['ez_pay'] == $option['id']):
							$total += $order['order_total'];
						break;
					endif;
				endforeach;

			else:

			$total += $order['order_total'];

			endif;

		endforeach;




		return $total;
	}

	function get_ezpay($ez_id = '')
	{
		$data = array();
	    $data1 = array();

		if($ez_id == ''):

			$sql = "SELECT
								*
					FROM
								checkout_ezpay ce";
		else:

			$sql = "SELECT
								*
					FROM
								checkout_ezpay ce
					WHERE
								id = '$ez_id'";

		endif;

		$data = $this->Execute($sql);



		return $data;
	}

	public function mabs15()
	{
		# This method will return all orders
		$data = array();
		$data1 = array();
		$data2 = array();
		$data3 = array();


		# Get all MABS15 orders
		$sql = "SELECT
							*
				FROM
							checkout_customers cc,
							checkout_orders co
				WHERE
							cc.customer_id = co.customer_id AND
							co.order_product_id LIKE 'MABA-%' AND
							co.order_id > '$order_id'";

		$data = $this->Execute($sql);



		# Get all MABS15 Ticket
		$sql = "SELECT
							*
				FROM
							checkout_orders co,
							checkout_products_join cpj
				WHERE
							cpj.product_id = 12 AND
							co.order_product_id LIKE 'MABA-%' AND
							co.order_id = cpj.order_id AND
							co.order_id > '$order_id'";

		$data1 = $this->Execute($sql);

		$ez = $this->get_ezpay();


		for($i = 0; $i < sizeof($data); $i++):
			for($j = 0; $j < sizeof($data1); $j++):

				if($data[$i]['order_id'] == $data1[$j]['order_id']):
					$data[$i]['cpj_ticket'] = '1';
					$data[$i]['cpj_ticket_cost'] = $data1[$j]['actual_cost'];
					break;
				else:
					$data[$i]['cpj_ticket'] = '0';
				endif;

			endfor;
		endfor;





		return $data;
	}
}


?>
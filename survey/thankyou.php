<?php
	/*date_default_timezone_set("America/New_York");
	$now = date("Y-m-d H:i:s");


    require('program/program.php');
	require('program/definitions.php');
	require('program/class.checkout.php');
	require('program/functions.php');

	$db = new Checkout(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->open();

	$states = $db->get_all_states();

	$countries = $db->get_all_countries();



	$db->close();

	*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Martial Arts Business Accelerator by Mike Parrella</title>


    <!-- Custom Google Web Font -->
    <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

	<link href="css/styles.css" rel="stylesheet" >

<script src="https://www.ilovekickboxing.com/intl_js/jquery.js"></script>



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-29420623-22', 'martialartsbusinessaccelerator.com');
  ga('send', 'pageview');

</script>



</head>
<body>

	<div class="container">

		<header>
			<img src="images/maba-logo.png" alt="" />
			<div class="clear"></div>
		</header>

		<div id="page_content" style="text-align: center;">

			<h1>Thank You!</h1>

			<p>Will be in touch with you shortly. Make sure you added us on facebook and have joined the
			special Facebook Group.
			</p>


		</div>

		<footer>
		<p class="privacy" style="text-align: center; font-size: 13px; color: #383838;">Copyright &copy; Parrella Consulting <?php echo date('Y'); ?>. All Rights Reserved</p>
	</footer>

	</div>


</body>

</html>

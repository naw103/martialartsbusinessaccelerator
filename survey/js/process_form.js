$(document).ready(function(){

	//$('input[type="checkbox"]').attr('checked', false);

	$('button#submit').click(function(){


		if(form_validate())
		{
			var customer_id = '';
			var name = $('input[name="name"]').val();
			//var email_address = 'james@fconlinemarketing.com';
			var most_improvement = $('input[name="most_improvement"]:checked').val();
			var most_improvement_other = $('textarea[name="most_improvement_other"]').val();
			var current_member_count = $('input[name="current_member_count"]:checked').val();
			var new_members = $('input[name="new_members"]:checked').val();
			var marketing_techniques = $('textarea[name="marketing_techniques"]').val();
			var avg_gross = $('textarea[name="avg_gross"]').val();
			var operation_cost = $('textarea[name="operation_cost"]').val();
			var website = $('input[name="website"]:checked').val();
			var website_traffic = $('textarea[name="website_traffic"]').val();
			var weekly_hours = $('select[name="weekly_hours"]').val();
			var fulltime_employees = $('input[name="fulltime_employees"]:checked').val();
			var parttime_employees = $('input[name="parttime_employees"]:checked').val();
			var fulltime_pay = $('textarea[name="fulltime_pay"]').val();
			var parttime_pay = $('textarea[name="parttime_pay"]').val();
			var yourself_pay_employee = $('textarea[name="yourself_pay_employee"]').val();
			var yourself_pay_owner =$('textarea[name="yourself_pay_owner"]').val();
			var business_years = $('select[name="business_years"]').val();;
			var unique_id = $('input[name="unique_id"]').val();


			$.ajax({
					url: 'ajax/process_form.php',
					dataType: 'xml',
					type: 'POST',
					data:
					{
						//customer_id: customer_id,
						fullname: name,
						//email_address: email_address,
						most_improvement: most_improvement,
						most_improvement_other: most_improvement_other,
						current_member_count: current_member_count,
						new_members: new_members,
						marketing_techniques: marketing_techniques,
						avg_gross: avg_gross,
						operation_cost: operation_cost,
						website: website,
						website_traffic: website_traffic,
						weekly_hours: weekly_hours,
						fulltime_employees: fulltime_employees,
						parttime_employees: parttime_employees,
						fulltime_pay: fulltime_pay,
						parttime_pay: parttime_pay,
						yourself_pay_employee: yourself_pay_employee,
						yourself_pay_owner: yourself_pay_owner,
						business_years: business_years,
						unique_id: unique_id
					},
					complete: function(xml,status)
					{
						notify_user(xml.responseText);
					}
			});
		}

	});


		function notify_user(xml)
		{
			var success = '';
			var response_text = '';

			$(xml).find('data').each(function(){

				success = $(this).find('success').text();
				response_text = $(this).find('response_text').text();
			});

			if(success == '1')
			{
				window.location.href = 'thankyou.php';
			}
			else
			{
				alert(response_text);
			}
		}




		function form_validate()
		{
			$('select, textarea, input[type="text"]').each(function(){

				if ($(this).val() == '')
				{
					$(this).parents('.wrapper').removeClass('valid');
					$(this).parents('.wrapper').addClass('error');
				}
				else
				{
					$(this).parents('.wrapper').removeClass('error');
					$(this).parents('.wrapper').addClass('valid');
				}

			});

			$('input[type="checkbox"]').each(function(){

				var attr_name = $(this).attr('name');


				if(! $('input[name="' + attr_name + '"]').is(':checked'))
				{
					$(this).parents('.wrapper').removeClass('valid');
					$(this).parents('.wrapper').addClass('error');

				}
				else
				{

					$(this).parents('.wrapper').removeClass('error');
					$(this).parents('.wrapper').addClass('valid');
				}

			});

			if($('input[name="most_improvement"]:checked').val() != 'Other' && $('input[name="most_improvement"]').is(':checked'))
			{
				$('textarea[name="most_improvement_other"]').parents('.wrapper').removeClass('error');
			}


			if($('.error').length > 0)
			{
				var pos = '';
				$('.error').each(function(){

					var tmp = $(this).index('.error') + 1;

					pos += tmp + ', ';

				});

				//alert("Questions " + pos + " are missing");
				alert("Must answer all questions");
				return false;
			}

			return true;
		}

		$('input[type="checkbox"]').click(function(){

	    	var attr_name = $(this).attr('name');
			console.log(attr_name);
			if($('input[name="' + attr_name + '"]').is(':checked'))
			{

			   $('input[name="' + attr_name + '"]').attr('checked', false);
			   $(this).attr('checked', true);
			}

			if($('input[name="most_improvement"]:checked').val() == 'Other')
			{
				$('textarea[name="most_improvement_other"]').show();
			}
			else
			{
				$('textarea[name="most_improvement_other"]').hide();
			}

			if(!$('input[name="' + attr_name + '"]').is(':checked'))
			{
					$(this).parents('.wrapper').removeClass('valid');
					$(this).parents('.wrapper').addClass('error');

				}
				else
				{

					$(this).parents('.wrapper').removeClass('error');
					$(this).parents('.wrapper').addClass('valid');
				}


		});


	$('select, textarea, input[type="text"]').focus(function(){

		$(this).parents('.wrapper').removeClass('valid');

	});

	$('select, textarea, input[type="text"]').blur(function(){

				if ($(this).val() == '')
				{
					$(this).parents('.wrapper').removeClass('valid');
					$(this).parents('.wrapper').addClass('error');
				}
				else
				{
					$(this).parents('.wrapper').removeClass('error');
					$(this).parents('.wrapper').addClass('valid');
				}

	});

	$('select').change(function(){
		if ($(this).val() == '')
				{
					$(this).parents('.wrapper').removeClass('valid');
					$(this).parents('.wrapper').addClass('error');
				}
				else
				{
					$(this).parents('.wrapper').removeClass('error');
					$(this).parents('.wrapper').addClass('valid');
				}
	});



});
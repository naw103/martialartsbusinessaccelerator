<?php

$jt_data = '';

$success = 0;
$response_text = '';


// Prep Data
foreach($_REQUEST as $key => $value):
	$$key = trim($value);
	$jt_data .= $key . ' => ' . $value . "\n";
endforeach;

//mail('james@fconlinemarketing.com','maba questionaire', $jt_data);



require('../program/definitions.php');
require('../program/class.database.php');

$db = new database(DB_HOST, DB_USER, DB_PASS, DB_NAME);
$db->open();

date_default_timezone_set("America/New_York");
$today = date('Y-m-d H:i:s');

# Get Customer Record
$sql = "SELECT
					*
		FROM
					checkout_customers cc
		WHERE
					unique_id = '$unique_id'";

$data = $db->Execute($sql);

# Get Customer ID
$customer_id = $data['customer_id'];

# Insert into database
$sql = "INSERT INTO maba_questionaire (
												   customer_id, name, email_address, most_improvement, most_improvement_other,
												   current_member_count, new_members, marketing_techniques, avg_gross, operation_cost,
												   website, website_traffic, weekly_hours, fulltime_employees, parttime_employees,
												   fulltime_pay, parttime_pay, yourself_pay_employee, yourself_pay_owner, business_years,
												   date_submitted
												)
										VALUES
												(
													'%s','%s','%s','%s','%s',
													'%s','%s','%s','%s','%s',
													'%s','%s','%s','%s','%s',
													'%s','%s','%s','%s','%s',
													'%s'
												)";
$sql = sprintf($sql,
					mysql_real_escape_string($customer_id),
					mysql_real_escape_string($fullname),
					mysql_real_escape_string(strtolower($email_address)),
					mysql_real_escape_string($most_improvement),
					mysql_real_escape_string($most_improvement_other),

					mysql_real_escape_string($current_member_count),
					mysql_real_escape_string($new_members),
					mysql_real_escape_string($marketing_techniques),
					mysql_real_escape_string($avg_gross),
					mysql_real_escape_string($operation_cost),

					mysql_real_escape_string($website),
					mysql_real_escape_string($website_traffic),
					mysql_real_escape_string($weekly_hours),
					mysql_real_escape_string($fulltime_employees),
					mysql_real_escape_string($parttime_employees),

					mysql_real_escape_string($fulltime_pay),
					mysql_real_escape_string($parttime_pay),
					mysql_real_escape_string($yourself_pay_employee),
					mysql_real_escape_string($yourself_pay_owner),
					mysql_real_escape_string($business_years),

					($today)
			);


$result = $db->Execute($sql);


$success = $result;

$db->close();



# Notify user.
header("Content-type: text/xml");
header("Cache-control: no-cache");

echo "<?xml version=\"1.0\" />\n";
echo "<response>\n";

echo "\t<data>\n";

echo "\t\t<success>" . $success . "</success>\n";
echo "\t\t<response_text>" . $response_text . "</response_text>\n";

echo "\t</data>\n";

echo "</response>\n";

?>